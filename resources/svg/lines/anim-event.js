/*! AnimEvent v0.0.8 (c) anseki https://github.com/anseki/anim-event */
var AnimEvent = function (n) {
    function e(o) {
        if (t[o]) return t[o].exports;
        var r = t[o] = {
            i: o,
            l: !1,
            exports: {}
        };
        return n[o].call(r.exports, r, r.exports, e), r.l = !0, r.exports
    }
    var t = {};
    return e.m = n, e.c = t, e.i = function (n) {
        return n
    }, e.d = function (n, t, o) {
        e.o(n, t) || Object.defineProperty(n, t, {
            configurable: !1,
            enumerable: !0,
            get: o
        })
    }, e.n = function (n) {
        var t = n && n.__esModule ? function () {
            return n.default
        } : function () {
            return n
        };
        return e.d(t, "a", t), t
    }, e.o = function (n, e) {
        return Object.prototype.hasOwnProperty.call(n, e)
    }, e.p = "", e(e.s = 0)
}([function (n, e, t) {
    "use strict";

    function o() {
        var n, e;
        i && (c.call(window, i), i = null), w.forEach(function (e) {
            e.event && (e.listener(e.event), e.event = null, n = !0)
        }), n ? (m = Date.now(), e = !0) : Date.now() - m < a && (e = !0), e && (i = l.call(window, o))
    }

    function r(n) {
        var e = -1;
        return w.some(function (t, o) {
            return t.listener === n && (e = o, !0)
        }), e
    }
    Object.defineProperty(e, "__esModule", {
        value: !0
    });
    var i, u = 1e3 / 60,
        a = 500,
        l = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame || function (n) {
            setTimeout(n, u)
        },
        c = window.cancelAnimationFrame || window.mozCancelAnimationFrame || window.webkitCancelAnimationFrame || window.msCancelAnimationFrame || function (n) {
            clearTimeout(n)
        },
        w = [],
        m = Date.now(),
        f = {
            add: function (n) {
                var e;
                return r(n) === -1 ? (w.push(e = {
                    listener: n
                }), function (n) {
                    e.event = n, i || o()
                }) : null
            },
            remove: function (n) {
                var e;
                (e = r(n)) > -1 && (w.splice(e, 1), !w.length && i && (c.call(window, i), i = null))
            }
        };
    e.default = f, n.exports = e.default
}]);