xdom.datagrid = {};
xdom.datagrid.columns = {};

xdom.tracking.attributes = ["value", "x:deleting", "x:checked"];
xdom.tracking.prefixes = [undefined];
xdom.deprecated = {};
xdom.tools = {};
xdom.xhr = {};
xdom.xhr.cache = {};
xdom.xhr.Requests = {};
xdom.xml.namespaces["requesting"] = "http://panax.io/fetch/requesting"
xdom.xml.namespaces["upload"] = "http://panax.io/xdom/upload"
xdom.xml.namespaces["copy"] = "http://panax.io/commands/copy"
xdom.xml.namespaces["exsl"] = "http://exslt.org/functions"
xdom.xml.namespaces["math"] = "http://exslt.org/math"
xdom.xml.namespaces["router"] = "http://panax.io/xdom/router"
xdom.xml.namespaces["sql"] = "http://panax.io/xdom/sql"
xdom.xml.namespaces["sp"] = "http://panax.io/xdom/sql/procedure"
xdom.xml.namespaces["fn"] = "http://panax.io/xdom/sql/function"
xdom.xml.namespaces["xhr"] = "http://panax.io/xdom/xhr"
xdom.xml.namespaces["prev_source"] = "http://panax.io/fetch/request/previous"
xdom.xml.namespaces["load"] = "http://panax.io/fetch/request"
xdom.xml.namespaces["context"] = "http://panax.io/context"
xdom.xml.namespaces["xdom"] = "http://panax.io/xdom"

xdom.messages.alert = xdom.dom.alert

xdom.listener.on('beforeHashChange', function (new_hash, old_hash) {
    var new_hash = (new_hash || window.location.hash);
    var page = (new_hash.match(/#(\d+)$/) || [])[1]
    if (page) {
        var data = xdom.stores.active.selectSingleNode("/*[1]");
        var request = px.getEntityInfo(data);
        request["pageIndex"] = page;
        px.request(request);
    }
    if (!(new_hash in xdom.stores)) {
        event.stopPropagation();
    }
    //xdom.stores.active = (xdom.stores[new_hash] || xdom.stores.active);
})
var px = {};
px.fetch = async function () {
    return await xdom.fetch.apply(this, arguments).then(response => [response.body, response.request]);
}
px.data = {};
px.dom = {};
xdom.data.Store = xdom.Store;
xdom.data.stores = xdom.stores;

xdom.xml.namespaces["px"] = "http://panax.io";
xdom.data.hashTagName = function (document) {
    var node = (document || xdom.stores.active).selectSingleNode("/*[1]");
    if (node) {
        return '#' + Array.prototype.coalesce(node.getAttribute("x:tag"), node.localName.toLowerCase(), "");
    } else {
        return '#';
    }
}

px.data.hashTagName = function (document) {
    var data = (document || xdom.stores.active).selectSingleNode("/*[1]");
    var entity = px.getEntityInfo(data);
    if (entity != undefined && data) {
        return '#' + (entity["schema"] + '/' + entity["mode"] || 'view').toLowerCase() + ':' + data.localName.toLowerCase();
    } else if (data) {
        return '#' + xdom.data.coalesce(data.getAttribute("x:tag"), data.getAttribute("x:id"), data.localName.toLowerCase());
    } else {
        return '#';
    }
}
xdom.data.hashTagName = px.data.hashTagName;

xdom.Store.prototype.generateTag = function () {
    let store = this;
    var data = store.selectSingleNode("/*[1]");
    var entity = px.getEntityInfo(data);
    if (entity != undefined && data) {
        return (entity["schema"] + '/' + entity["mode"] || 'view').toLowerCase() + ':' + data.localName.toLowerCase();
    } else if (data) {
        return xdom.data.coalesce(data.getAttribute("x:tag"), data.getAttribute("x:id"), data.localName.toLowerCase()).split(/^#/).pop();
    } else {
        return '';
    }
}

px.data.checkAll = function (id, checked) {
    var data_rows = xdom.data.find(id).selectNodes('px:data/px:dataRow');
    xdom.data.setAttribute(data_rows, "@x:checked", String(checked))
    xdom.dom.refresh()
}
xdom.data.checkAll = px.data.checkAll;

xdom.listener.on('attributeChanged', function ({ node, attribute, value }) {
    if (["mode", "pageSize", "pageIndex", "filters"].includes(attribute) && xdom.stores.active.document.contains(node) && node.ownerDocument.documentElement.getAttribute("mode") && node.ownerDocument.documentElement.getAttribute("Name")) {
        px.request(px.getEntityInfo(node.ownerDocument));
    }
})

px.getEntityInfo = function (input_document) {
    var current_document = (input_document || ((event || {}).target || {}).store || xdom.stores[(window.location.hash || "#")]);
    if (!current_document) return undefined;
    var entity;
    current_document = (current_document.documentElement || current_document)
    if (current_document && current_document.getAttribute && current_document.getAttribute("mode") && current_document.getAttribute("Name")) {
        entity = {}
        entity["schema"] = current_document.getAttribute("Schema");
        entity["name"] = current_document.getAttribute("Name");
        entity["mode"] = current_document.getAttribute("mode");
        entity["pageIndex"] = current_document.getAttribute("pageIndex");
        entity["pageSize"] = current_document.getAttribute("pageSize");
        entity["filters"] = (current_document.getAttribute("filters") || ''); //Se reemplazan las comillas simples por dobles comillas simples. Revisar si esto se puede hacer en px.request
    }
    return entity;
}

px.removeRecord = function (ref) {
    var row = xdom.data.find(ref);
    if (!row.getAttribute('identity')) {
        xdom.data.remove(row);
        xdom.dom.refresh();
    } else {
        row.setAttribute('x:deleting', 'true');
    }
}


xdom.listener.on('xmlTransformed', function ({ original, transformed }) {
    if (!(original && transformed)) { return; }
    let px_original = px.getEntityInfo(original);
    let px_transformed = px.getEntityInfo(transformed);
    if (px_original && px_transformed && px_transformed["filters"] != px_original["filters"]) {
        xdom.delay(50).then(() => px.request(px_transformed));
    }
})

Object.defineProperty(xdom.manifest, 'getConfig', {
    value: function (entity_name, property_name) {
        if (entity_name.match(/#(.+)\/(.+):(.+)/)) {
            const [, schema, mode, name] = entity_name.match(/#(.+)\/(.+):(.+)/);
            return (Object.values(xdom.manifest.modules.filter((key, value) => {
                return key.match(new RegExp(`#(${schema})\/(.*[\[\|]${mode}\\b.*\]|${mode}):(${name})`, 'gi')) && (value || {})[property_name]
            }))[0] || Object.values(xdom.manifest.modules.filter((key, value) => {
                return key.match(new RegExp(`#(${schema})\/\\*:(${name})`, 'gi')) && (value || {})[property_name]
            }))[0] || {})[property_name]
        } else {
            return ((xdom.manifest.modules || {})[entity_name] || {})[property_name];
        }
    },
    writable: true, enumerable: false, configurable: false
});

px.requestRoutine = async function (name, parameters, tag) {
    let document = await xdom.fetch.xml(xdom.manifest.server.endpoints.request + `?command=${name}${parameters ? "&parameters=" + parameters : ""}`);
    if (document && document.documentElement) {
        xdocument = new xdom.Store(document, { tag: document.documentElement.getAttribute("x:tag") || tag || name.replace(/[\s:~/\\;\?\$&%@^=\*\(\)\'"`\{\}\[\]><]/gm, '') });

        if (!xdocument.stylesheets.length) {
            xdocument.addStylesheet({ href: 'datagridview.xslt', target: '@#shell main' });
        }
        xdom.stores.active = xdocument;
    } else {
        alert("No hay resultados")
    }
}

app = {}
app.request = async function (req, parameters = {}, tag) {
    var name, options = {}, payload, onSuccess, onException;
    if (req.constructor == {}.constructor) {
        parameters = xdom.json.merge({ command: (req["request"] || req["command"]) }, req.parameters);
        name = parameters["command"];
        onSuccess = req["onSuccess"];
        onException = req["onException"];
        onComplete = req["onComplete"];
        payload = req["payload"];
        delete req["onSuccess"];
        delete req["onException"];
        delete req["onComplete"];
        delete req["request"];
        delete req["parameters"];
        delete req["payload"];
        delete req["type"];
        options = (req["xhr"] || req);
        delete options["type"];
    } else {
        name = req;
        if (typeof (parameters) == 'string') {
            if (parameters.match(/@[^=]+=/ig)) {
                parameters = Object.fromEntries(new URLSearchParams(parameters).entries())
            } else {
                parameters = { parameters: parameters };
            }
        }
        parameters = xdom.json.merge({ command: name }, parameters);
    }
    this.parameters = parameters;

    let current_store = xdom.stores.active;
    try {
        options["tag"] = options["tag"] || name.replace(/[\s:~/\\;\?\$&%@^=\*\(\)\'"`\{\}\[\]><]/gm, '');
        current_store.state.loading = true;
        let document = await xdom.server.request(parameters, options, payload);
        current_store.state.loading = false;
        if (!(document instanceof xdom.Store) && document instanceof XMLDocument && document.documentElement) {
            document = new xdom.Store(document, { tag: document.documentElement.getAttribute("x:tag") || name.replace(/[\s:~/\\;\?\$&%@^=\*\(\)\'"`\{\}\[\]><]/gm, '') });

            if (document.stylesheets.length) {
                //document.addStylesheet({ href: 'datagridview.xslt', target: '@#shell main' });
                xdom.stores.active = document;
            }
        }
        if (onSuccess && onSuccess.apply) {
            let response = new Response()
            Object.defineProperty(response, 'type', {
                get: () => {
                    return "xml"
                }
            })
            Object.defineProperty(response, 'document', {
                get: () => {
                    return document.document || document
                }
            })
            Object.defineProperty(response, 'documentElement', {
                get: () => {
                    return document.document.documentElement || document
                }
            })
            onSuccess.apply(this, [response, this]);
        } else {
            return Promise.resolve(document);
        }
    } catch (document) {
        current_store.state.loading = false;
        if (onException && onException.call) {
            let response = new Response()
            Object.defineProperty(response, 'type', {
                get: () => {
                    return "xml"
                }
            })
            Object.defineProperty(response, 'document', {
                get: () => {
                    return document.document || document
                }
            })
            onException.apply(this, [response, this]);
        } else {
            if (document instanceof HTMLDocument) {
                return Promise.reject(xdom.dom.createDialog(document));
            } else {
                return Promise.reject(document);
            }
        }
    }
}

px.request = async function (request_or_entity_name, mode, filters, ref) {
    if (!request_or_entity_name) {
        return null;
    }
    if (!(xdom.manifest.server["endpoints"] && xdom.manifest.server["endpoints"]["request"])) {
        throw ("Endpoint for request is not defined in the manifest");
    }
    var schema, entity_name;
    var page_index, page_size;
    var on_success = function (xml_document) { xdom.stores.active = xml_document; };
    if (typeof (request_or_entity_name) == 'string') {
        try {
            [, schema, mode, entity_name] = request_or_entity_name.match(/#(.+)\/(.+):(.+)/);
        } catch (e) {
            [, schema, entity_name] = request_or_entity_name.match(/\[?([^\]]+)\]?\.\[?([^\]]+)\]?/);
        }
        mode = (mode || "view");
        if (request_or_entity_name == "[Historico].[Tendencias]") {
            page_size = "5000"
        }
    }
    if (request_or_entity_name.constructor === {}.constructor) {
        ({ schema, name: entity_name } = request_or_entity_name);
        //schema = request_or_entity_name["schema"]
        //entity_name = (schema ? "[" + schema + "]." : "") + request_or_entity_name["name"];
        if (!schema) {
            let full_name = entity_name;
            [, schema, entity_name] = String(full_name).match(/^\s*\[([^\]]+)\]\.\[(.+)\]\s*$/i);
        }
        mode = (mode || request_or_entity_name["mode"] || "view");
        page_index = request_or_entity_name["pageIndex"];
        filters = (request_or_entity_name["filters"] || filters);
        on_success = (request_or_entity_name["on_success"] || on_success);
    }
    filters = (filters || xdom.manifest.getConfig(`#${schema}/${mode}:${entity_name}`, "filters") || "").replace(/'/g, "''");

    page_size = (page_size || xdom.manifest.getConfig(`#${schema}/${mode}:${entity_name}`, "pageSize"));
    page_index = (page_index || xdom.manifest.getConfig(`#${schema}/${mode}:${entity_name}`, "pageIndex"));
    var ref = ref;
    //var current_location = window.location.hash.match(/#(\w+):(\w+)/);
    var rebuild = (!xdom.listener.keypress.altKey ? 'DEFAULT' : '1');
    let current_store = xdom.stores.active;
    current_store.state.loading = true;
    try {
        let Response = await xdom.server.request(`command=[$Ver:Beta_12].getXmlData @@user_id='-1', @full_path='', @table_name='[${schema}].[${entity_name}]', @mode=${(!mode ? 'DEFAULT' : `'${mode}'`)}, @page_index=${(page_index || 'DEFAULT')}, @page_size=${(page_size || 'DEFAULT')}, @max_records=DEFAULT, @control_type=DEFAULT, @Filters=${(!filters ? 'DEFAULT' : `'${Encoder.urlEncode(filters)}'`)}, @sorters=DEFAULT, @parameters=DEFAULT, @lang=es, @get_data=1, @get_structure=1, @rebuild=${rebuild}, @column_list=DEFAULT, @output=HTML`, {
            headers: {
                "Content-Type": 'text/xml'
                , "Accept": 'text/xml'
                , "X-Detect-Input-Variables": false
                , "X-Detect-Output-Variables": false
                , "X-Debugging": xdom.debug.enabled
            }
        })
        Request.requester = ref;

        if (!(Response instanceof xdom.Store) && Response && Response.documentElement) {
            var manifest_stylesheets = (xdom.manifest.getConfig(xdom.data.hashTagName(Response.documentElement), 'transforms') || []).filter(t => !(t.role == 'init' || t.role == "binding"));
            var stylesheets = manifest_stylesheets.concat([{ href: (Response.documentElement.getAttribute('controlType') || "shell").toLowerCase() + '.xslt' }].filter(() => (manifest_stylesheets.length == 0))).reduce((stylesheets, transform) => { stylesheets.push({ "href": transform.href, target: (transform.target || '@#shell main'), role: transform.role }); return stylesheets; }, []);
            stylesheets.map(stylesheet => Response.addStylesheet(stylesheet));
            current_store.state.loading = false;
            new xdom.Store(Response, function () {
                var caller = xdom.stores.find(Request.requester)[0];
                xml_document = this;
                //if (xml_document) {
                //    xdom.xhr.cache[request];
                //    if (cache_results) {
                //        xdom.cache[request] = xml_document;
                //    }
                //}
                xml_document.reseed();
                if (caller && caller.selectSingleNode('self::px:dataRow')) {
                    var new_datarow = xml_document.document.selectSingleNode('*/px:data/px:dataRow')
                    if (new_datarow) {
                        new_datarow.setAttribute('x:id', caller.getAttribute('x:id'))
                    }
                }
                if (caller && caller.selectSingleNode('(ancestor-or-self::*[@Name and @Schema][1])[@foreignReference]')) {
                    xml_document.document.documentElement.setAttribute('x:reference', caller.getAttribute('x:id'), false)
                    var foreignReference = caller.selectSingleNode('ancestor-or-self::*[@foreignReference]');
                    if (foreignReference) {
                        xml_document.documentElement.selectAll('//px:layout//px:field[@fieldName="' + foreignReference.getAttribute('foreignReference') + '"]').remove(false);
                    }
                }
                var transforms = (xdom.manifest.getConfig(xdom.data.hashTagName(xml_document.document), 'transforms') || []).filter(stylesheet => stylesheet.role == 'init').map(stylesheet => stylesheet.href);
                transforms.push('xdom/panax/panax_bindings.xslt');
                for (transform of transforms) {
                    xml_document.document = xdom.xml.transform(xml_document.document, transform);
                }
                if (on_success) {
                    on_success.apply(this, [xml_document]);
                }
                //xdom.delay(50).then(() => {
                xdom.stores.active = this;
                //});
            }, Response, Request);
        }
    } catch (e) {
        current_store.state.loading = false;
        if (e instanceof HTMLDocument) {
            return Promise.reject(xdom.dom.createDialog(e));
        } else {
            return Promise.reject(e);
        }
    }
}

Object.defineProperty(xdom.server, 'login', {
    value: async function (username, password, database_id/*, target_node*/) {
        if (xdom.session["status"] == "authorizing") return;
        var database_id = (database_id || xdom.session.database_id || xdom.manifest.server.database_id || window.location.hostname);
        password = username !== undefined ? (password || xdom.data.coalesce(password != undefined && xdom.cryptography.encodeMD5(password), password) || "") : "";
        username = (username || "");
        /*target_node = (target_node || xdom.stores.active.selectSingleNode('//*[@session:status][1]') || xdom.stores.active.selectSingleNode('/*'));*/

        //if (!(((xdom.manifest.server || {}).endpoints || {}).login)) {
        //    return Promise.reject("Login endpoint not configured.");
        //}
        xdom.session["status"] = "authorizing";
        xdom.session["user_login"] = (username || "");
        //var messages = (target_node && target_node.selectNodes('*[local-name()="message" and namespace-uri()="http://panax.io/xdom"][@scope="login"]') || document.createElement('p'));
        //messages.remove();
        var url = new URL(relative_path + xdom.manifest.server["endpoints"]["login"], location.origin + location.pathname);
        return xdom.post.to(url, new URLSearchParams(`UserName=${username}&Password=${password}&database_id=${database_id}`))
            .then(response => [response.body, response])
            .then(async ([response, Response]) => {
                //xdom.state.update({ active: undefined }, "");
                xdom.session.updateSession({
                    "user_id": (response.userId || '')
                    , "user_login": (response.user_login || '').toLowerCase()
                    , "user_name": (response.user_name || '').toLowerCase()
                    , "database_id": (response.database_id || database_id || '')
                    , "status": (response.status || Response.ok && "authorized" || "unauthorized")
                    , "userId": response.userId
                });
                //if ((target_node.documentElement || target_node).tagName == 'x:empty') {
                //    xdom.stores.active = null;
                //}
                //xdom.data.remove(messages);
                //try {
                //    await xdom.init();
                //} catch (message) {
                //    console.log(message)
                //}
                if (Response.ok && xdom.state.seed == '#login') {
                    xdom.stores.active = xdom.data.default;
                }
                //await xdom.data.binding.trigger();
                if (Response.ok) {
                    console.info('Welcome to your session!');
                }
                if (response.recordSet) {
                    result = response.recordSet[0].Result;
                    //if (cache_results) {
                    //    xdom.cache[request] = result;
                    //}
                    target.setAttribute(target_attribute, result);
                }
                if (response.message) {
                    xdom.messages.alert(response.message);
                }
                xdom.stores.active.render(/*true*/);
                return response
            }).catch(response => {
                if (response) {
                    switch (response.bodyType) {
                        case 'json':
                        case 'text':
                            let message = xdom.data.coalesce(response.body.message, response.body);
                            if (message) {
                                xdom.messages.alert(message)
                            }
                            break;
                        case 'xml':
                            xdom.stores.active.documentElement.appendChild(response.documentElement || response);
                            break;
                    }
                }
                xdom.session.status = "unauthorized";
                return response
            }).finally(() => {
                //xdom.dom.refresh();
                if (xdom.debug["xdom.session.login"]) {
                    console.log("\tCompleted: (" + request + ') = ' + result);
                }
                if (xdom.session.getKey("status") == 'unauthorized') {
                    return Promise.reject("Unauthorized");
                } else {
                    return Promise.resolve(true)
                }
            });
        //xdom.dom.refresh();

        //oData.onSuccess = function (Response, Request) {
        //}

        //oData.onException = function (Response, Request) {
        //}
        //oData.onFail = oData.onException;
        //oData.load();
    },
    writable: false, enumerable: false, configurable: false
});

Object.defineProperty(xdom.server, 'submit', {
    value: async function (settings) {
        let self = this;
        await self.library.load();
        //if (arguments.length == 1 && arguments[0].constructor === {}.constructor) {
        //    settings = arguments[0];
        //} else if (arguments.length == 1 && typeof arguments[0] == 'string') {
        //    var uid = arguments[0];
        //    xdom.stores.active = xdom.xml.transform(xdom.stores.active, xdom.library["xdom/resources/normalize_namespaces.xslt"]);
        //    data = xdom.stores.active.selectSingleNode('//*[@x:id="' + uid + '"]');
        //}
        //data = (data.documentElement || data);
        data = self.document.documentElement.cloneNode(true);
        this.prepareData = function (data) {
            if (settings["prepareData"] && settings["prepareData"].apply) {
                settings["prepareData"].apply(this, [data])
            };
            return data;
        }

        var settings = (settings || {});
        var xhr_settings = xdom.json.merge({ headers: { "Accept": 'text/xml', "Content-Type": 'text/xml' } }, (xhr_settings || settings["xhr"]));

        self.state.submitting = true;
        await self.render(/*true*/);
        //for (let datarow of datarows) {
        //    datarow.setAttribute("@state:submitting", "true");
        //}
        //var onsuccess = (xhr_settings["onSuccess"] || function () { });
        if (data) {
            this.prepareData(data);

            data.setAttributes({
                "session:user_id": (data.getAttribute("session:user_id") || xdom.session.getUserId())
                , "session:user_login": (data.getAttribute("session:user_login") || xdom.session.getUserLogin())
                , "session:status": (xdom.session.getUserLogin() ? "authorized" : "unauthorized")
            });

            var target = data.ownerDocument.selectSingleNode('//*[@x:reference]')
            var datarows = data.selectNodes('px:data/px:dataRow');

            if (target && data.selectSingleNode('px:data/px:dataRow[not(@identity)]')) {
                let references = xdom.stores.find(target.getAttribute('x:reference')).map(source => {
                    if (source.ownerDocument.store == self) return;
                    var success;
                    for (let dr in datarows) {
                        var datarow = datarows[dr].cloneNode(true);
                        datarow.$$("//source:*|//@source:*|//x:r").remove();
                        var target = source.ownerDocument.store.find(datarow);
                        if (target) {
                            target.replace(datarow);
                        } else if (source.selectSingleNode('self::px:dataRow')) {
                            xdom.dom.insertAfter(datarow, source);
                        } else if (!source.selectSingleNode('px:data')) {
                            source.appendChild(datarow.parentNode);
                        }
                        else {
                            source.selectSingleNode('px:data').appendChild(datarow);
                        }
                        success = true;
                    }
                    //if (xdom.stores[[...xdom.state.prev].shift()] == source.ownerDocument.store) {
                    if (success) history.back();
                    //} else {
                    //    xdom.stores.active = source.ownerDocument;
                    //}
                    return success;
                })
                if (references.find(response => response)) {
                    return;
                }
                if (confirm("¿Desea guardar el registro en este momento?")) {
                    xdom.data.remove(target.selectSingleNode('@x:reference'));
                } else {
                    history.back();
                }
                //xdom.dom.navigateTo("#proveedor:edit");
            }

            var submit_data;
            if (data.getAttribute("transforms:submit")) {
                submit_data = xdom.xml.transform(data.ownerDocument, data.getAttribute("transforms:submit"));
            }
            var payload = xdom.xml.createDocument('<x:post xmlns:x="http://panax.io/xdom"><x:source>' + xdom.xml.toString(data) + '</x:source>' + (submit_data ? '<x:submit>' + xdom.xml.toString(submit_data) + '</x:submit>' : '') + '</x:post>');
            return await xdom.post.to(xdom.manifest.server["endpoints"]["post"], payload, xhr_settings).then(([document, request, response]) => {
                let content_type = response.headers.get("content-type");

                //var results = response.value;
                //if (onsuccess && onsuccess.apply) {
                //    onsuccess.apply(this, arguments);
                //}
                if (content_type.indexOf('json') != -1 || content_type.indexOf('javascript') != -1) {
                    results = document;
                    if (results && results.message) {
                        alert(results.message);
                    }
                    //console.log(document);
                    ////xdom.data.update(data.getAttribute("x:id"), "@x:trid", results.recordSet[0][""]);
                }
                else if (content_type.indexOf('xml') != -1) {
                    if (document.stylesheets.length == 1) { /*Por lo pronto sólo entrará para los errores y si tiene un solo stylesheet*/
                        var result = xdom.xml.transform(document);
                        if (result.documentElement && result.documentElement.namespaceURI.indexOf("http://www.w3.org") != -1) {
                            window.document.querySelector(document.stylesheets[0].target || "body").appendChild(result.documentElement);
                            appended = true;
                        }
                    } else if (document.selectAll("//@statusMessage").length) {
                        document.selectAll("//@statusMessage").map(message => xdom.messages.alert(message.value))
                    }
                    //else {
                    //    //for (let datarow of datarows) {
                    //    //    datarow.setAttribute("@state:submitting", undefined, false);
                    //    //}
                    //    __document.documentElement.setAttribute("state:submitted", "true");
                    //    window.history.back();
                    //}
                    ////if (document.documentElement && document.documentElement.selectSingleNode('/x:message')) {
                    ////    if (!(this.subscribers)) {
                    ////        (xdom.dom.findClosestDataNode(Request.srcElement) || xdom.stores.active.documentElement).appendChild(document.documentElement);
                    ////        xdom.dom.refresh({ after: function () { return null; } });
                    ////    } else {
                    ////        var target = xdom.stores.active;
                    ////        if (target) {
                    ////            target.documentElement.appendChild(document.documentElement);
                    ////        } else {
                    ////            console.warn(Response.responseXML)
                    ////        }
                    ////    }
                    ////}
                }

                //if (results && results.message) {
                //    alert(results.message);
                //}
                ////xdom.data.update(data.getAttribute("x:id"), "@state:submitted", "true", false);
                try {
                    window.top.dispatchEvent(new CustomEvent('submitSuccess', { detail: { srcStore: this, response: response } }));
                } catch (e) {
                    console.warn(e)
                }
                return [document, request, response];
            }).catch(([document, request, response]) => {
                if (response.status == 304) {
                    alert("No hay cambios");
                }
                else if (document.contentType.indexOf('xml') != -1) {
                    if (!response.appended && document.stylesheets.length == 1) { /*Por lo pronto sólo entrará para los errores y si tiene un solo stylesheet*/
                        var result = xdom.xml.transform(document);
                        if (result.documentElement && result.documentElement.namespaceURI.indexOf("http://www.w3.org") != -1) {
                            window.document.querySelector(document.stylesheets[0].target || "body").appendChild(result.documentElement);
                            appended = true;
                        }
                    }
                    //if (Response.document.documentElement.selectSingleNode('/x:message')) {
                    //    if (!(this.subscribers)) {
                    //        (xdom.dom.findClosestDataNode(Request.srcElement) || xdom.stores.active.documentElement).appendChild(Response.responseXML.documentElement);
                    //        xdom.dom.refresh({ after: function () { return null; } });
                    //    } else {
                    //        var target = xdom.stores.active;
                    //        if (target) {
                    //            target.documentElement.appendChild(Response.document.documentElement);
                    //        } else {
                    //            console.warn(Response.document)
                    //        }
                    //    }
                    //}
                } else if (response.status != 401) {
                    alert("No se pudo guardar la información, intente de nuevo");
                }
                return [document, request, response];
            }).finally(() => {
                self.state.submitting = undefined;
                self.state.loading = undefined;
                //xdom.data.update(data.getAttribute("x:id"), "@state:submitting", "false");
                for (let datarow of datarows) {
                    datarow.setAttribute("@state:submitting", undefined, false);
                }
                self.render(/*true*/);
            });
        }
    },
    writable: false, enumerable: false, configurable: false
});

Object.defineProperty(xdom.server, 'logout', {
    value: function (options) {
        //target_node = (target_node || xdom.stores.active.selectSingleNode('//*[@session:status][1]') || xdom.stores.active.selectSingleNode('/*'));
        var { auto_reload = true } = (options || {});

        return this.logout.call(this, {}, { method: 'POST', async: false, headers: { "Accept": 'application/json' } }).then((Response) => {
            if (Response.success) {
                for (let hashtag in sessionStorage) {
                    if (hashtag.indexOf("#") != -1) {
                        console.log('Clearing document ' + hashtag);
                        delete xdom.stores[hashtag];
                    }
                }
                xdom.session.setUserId(null);
                xdom.stores.clear();
                xdom.session.clearCache(options);
                var database_id = (xdom.session.database_id || xdom.session.getKey("database_id"));
                xdom.session.status = "unauthorized";

                xdom.session.setKey("database_id", database_id);
                xdom.xhr.cache = {}
                window.top.dispatchEvent(new CustomEvent('loggedOut', { detail: {} }));
                console.info('Session ended. Goodbye!')
            } else if (!Response.success && Response.message) {
                console.error(Response.message);
            }
            if (auto_reload) {
                window.location.href.replace(/#.*$/g, '');
                window.removeEventListener("beforeunload", xdom.dom.beforeunload);
                window.location.reload(true);
            }
            return true;
        });
    },
    writable: false, enumerable: false, configurable: false
});

let original_loadHash = xdom.data.loadHash;
xdom.data.loadHash = async function (hash) {
    await xdom.manifest.sources[hash] && xdom.manifest.sources[hash].fetch({ as: hash }) || await px.request(hash);
    if (!xdom.stores[hash]) {
        xdom.dom.navigateTo('#');
    }
}

px.modifyInlineRecord = function (original_request) {
    var request = original_request.cloneObject();
    var ref_record = xdom.data.findById(request["ref"])
    delete request["ref"];

    request["on_success"] = function (xDocument) {
        var node = xDocument.document.selectSingleNode('*/px:data/px:dataRow');
        ref_record = xdom.xml.createDocument(ref_record);
        if (ref_record.selectSingleNode('.//*[@initial:value!=@value]')) {
            node.parentNode.replaceChild(ref_record.documentElement, node);
        }
        xdom.stores.active = xDocument;
    }
    px.request(request, undefined, undefined, ref_record);//.selectSingleNode('ancestor::*[@Schema and @Name][1]'));
}

xdom.listener.on('submitSuccess', function ({ srcStore: store, response }) {
    let entity = px.getEntityInfo(store);
    if (entity) {
        switch (entity.mode) {
            case 'add':
            case 'edit':
                if (!(response.document.selectSingleNode(".//results/result[@status='error']"))) {
                    store.documentElement.setAttribute("state:submitted", "true");
                    if (((history.state || {}).prev || []).length) {
                        history.back();
                        window.top.dispatchEvent(new CustomEvent('navigatedBack', { bubbles: false, detail: { srcStore: store } }));
                    }
                }
                break;
            default:
        }
    }
})

xdom.listener.on('navigatedBack', function (event) {
    let next_store = xdom.stores[xdom.state.next];
    if (next_store) {
        let changes = next_store.selectNodes("//*[@state:submitted='true']");
        if (changes.length) {
            px.request(px.getEntityInfo());
        }
    }
})

xdom.listener.on('popstate', async function (event) {
    if (this.popping) {
        this.popping().cancel();
        //let current_hash = xdom.data.hashTagName();
        //history.replaceState({
        //    hash: current_hash
        //    , prev: ((history.state || {}).prev || [])
        //}, event.target.textContent, current_hash);
        this.popping = undefined;
    }
    function popstate() {
        let finished = false;
        let cancel = () => finished = true;
        xdom.session.database_id = xdom.session.database_id;
        const promise = new Promise((resolve, reject) => {
            setTimeout(async () => {
                var referencee, referencer, datarows;
                let next_store = xdom.stores[history.state.next];
                if (next_store instanceof xdom.Source) {
                    next_store = await next_store.fetch();
                }
                if (history.state && history.state.next && next_store instanceof xdom.Store) {
                    referencee = next_store.selectSingleNode("//*[@x:reference and //@state:submitted='true']");
                }
                if (referencee) {
                    datarows = referencee.selectNodes('px:data/px:dataRow');
                    referencer = xdom.data.find(referencee.getAttribute('x:reference'));
                }
                if (referencee && referencer && datarows) {
                    for (let datarow of datarows) {
                        xdom.data.replace(referencer, datarow);
                    }
                }
                resolve();
            }, 500);
            cancel = () => {
                if (finished) {
                    return;
                }
                reject();
            };

            if (finished) {
                cancel();
            }
        }).then((resolvedValue) => {
            this.popping = undefined;
            finished = true;
            return resolvedValue;
        }).catch((err) => {
            finished = true;
            return err;
        });
        return { promise, cancel }
    }
    this.popping = popstate;
    this.popping();
})

xdom.data.addRecord = function (ref_node, target_node, target_position, how_many) {
    ((event || {}).srcElement || document.createElement('p')).classList.add("working");
    var ref_id = xdom.stores.active.selectSingleNode(ref_node).getAttribute("x:id");
    xdom.data.history.undo.push(xdom.stores.active);
    xdom.data.updateNode(xdom.stores.active.find(ref_id), '@copy:after', (target_node || 'this()'), false).then(response => {
        xdom.stores.active = xdom.xml.transform(xdom.stores.active, "xdom/resources/commands.xslt");
        xdom.data.applyTransforms(xdom.stores.active.selectSingleNode(ref_node).selectSingleNode("ancestor-or-self::*[@x:transforms]"));
        xdom.dom.refresh();
    });
    xdom.dom.getScrollableElements();
}

xdom.data.duplicateRecord = function (ref_node, target_node, target_position, how_many) {
    ((event || {}).srcElement || document.createElement('p')).classList.add("working");

    if (typeof (ref_node) == 'string') {
        ref_node = xdom.stores.active.find(ref_node);
    }
    if (!ref_node) {
        return;
    }
    ref_node.setAttribute('@copy:after', (target_node || 'this()'), false);
    xdom.dom.getScrollableElements();
    xdom.stores.active = xdom.xml.transform(xdom.stores.active, "xdom/resources/commands.xslt");
}

xdom.data.deleteRecord = function (uid) {
    var uid = (uid || event.srcElement.id);
    target = xdom.data.findById(uid);
    xdom.dom.getScrollableElements();
    if (target.selectFirst('self::px:dataRow[@identity]')) {
        xdom.data.update(uid, '@x:deleting', 'true');
    } else {
        target.remove();
    }
}

xdom.data.removeRecord = function (uid) {
    var uid = (uid || event.srcElement.id);
    target = xdom.data.findById(uid);
    xdom.data.remove(target);
    xdom.dom.refresh();
}

xdom.data.undo = function () {
    xdom.stores.active.undo();
    /*if (xdom.data.history.undo.length == 0) return;
    xdom.data.history.redo.push(xdom.stores.active);
    xdom.stores.active = xdom.xml.createDocument(xdom.data.history.undo.pop());
    //xdom.stores.active.update = xdom.data.update;
    xdom.dom.refresh({ trigger_bindings: false }); //trigger bindings is disabled because it may modify xdom.data.history*/
}

xdom.data.redo = function () {
    xdom.stores.active.redo();
    /*if (xdom.data.history.redo.length == 0) return;
    xdom.data.history.undo.push(xdom.stores.active);
    xdom.stores.active = xdom.xml.createDocument(xdom.data.history.redo.pop());
    //xdom.stores.active.update = xdom.data.update;
    xdom.dom.refresh();*/
}

xdom.data.clearCache = function (cache_name, src) {
    var url = relative_path + xdom.manifest.server["endpoints"]["clearCache"] + "?file_name=" + cache_name.replace(/^[^:]+:/, '');
    var src = xdom.data.find((src || xdom.dom.findClosestElementWithId(event.srcElement).id.replace(/container_/i, '')));
    var oData = new xdom.xhr.Request(url);
    oData.onSuccess = function (Response, Request) {
        if (src) {
            xdom.data.remove(src.selectSingleNode('source:value'))
        }
        delete xdom.xhr.cache[Request.parameters["file_name"]];
        xdom.dom.refresh({ forced: true });
    }
    oData.onException = function (Response, Request) {
        result = Response;

    }
    oData.load();
}

xdom.data.toClipboard = function (source) {
    var dummyContent = xdom.xml.toString(source || xdom.stores.active);
    var dummy = (document.createElement('input'));
    dummy.value = dummyContent;
    document.body.appendChild(dummy);
    dummy.select();
    document.execCommand('copy');
    dummy.remove();
}

xdom.data.addRecord = function (ref_node, target_node, target_position, how_many) {
    var how_many = (how_many == undefined ? 1 : how_many)
    if (how_many <= 0) return;
    var ref_node = (ref_node || '/*/*[1]');
    var target_node = (target_node || '/*');
    var target_position = (target_position || 'first')
    var start_date = new Date();
    //xdom.data.clearSelection();
    xdom.data.history.undo.push(xdom.stores.active);
    var xNew;
    try {
        xNew = xdom.stores.active.selectSingleNode(ref_node).cloneNode(true)
    } catch (e) {
        xNew = xdom.data.getFirstRecord(xdom.stores.active).cloneNode(true);
    }
    xNew = xdom.xml.transform(xdom.xml.toString(xNew), xdom.library["xdom/resources/reset_record.xslt"]).selectSingleNode("*");
    var timestamp = Math.random().toString();
    xNew.setAttribute("timestamp", timestamp);
    xNew.setAttribute("editing", "true");
    xNew.setAttribute("x:selected", "true");
    xNew.removeAttribute("x:deleting");
    xdom.data.removeAttribute(xNew.selectNodes('*[@value]'), '@value');
    xdom.data.removeAttribute(xNew.selectNodes('*[@prev:value]'), '@prev:value');
    //div.innerHTML = xNew.outerHTML;
    //xdom.stores.active.appendChild(div.childNodes)
    ////xdom.stores.active.firstElementChild.insertAdjacentHTML('beforebegin', xNew); //Este método cambia las mayúsculas por minúsculas
    try {
        //var data = xdom.xml.createDocument(xdom.stores.active);
        var target = xdom.stores.active.selectSingleNode(target_node);
        if (target_position == 'first') {
            target.insertBefore(xNew, target.firstElementChild);
        } else if (target_position == 'previous') {
            target = xdom.stores.active.selectSingleNode(ref_node);
            xdom.dom.insertBefore(xNew, target);
        } else if (target_position == 'next') {
            target = xdom.stores.active.selectSingleNode(ref_node);
            xdom.dom.insertAfter(xNew, target);
        } else {
            xdom.dom.insertAfter(xNew, target.lastElementChild, target);
        }
        ////data.selectSingleNode('/*').appendChild(xNew, data.selectSingleNode('/*/*[1]'));
        //xdom.stores.active = xdom.xml.createDocument(data);
    } catch (e) {
        console.log(e.message);
        return;
    }
    if (xdom.debug["xdom.data.addRecord"]) {
        console.log("@addRecord#Transformation Time: " + (new Date().getTime() - start_date.getTime()));
    }
    var new_record_uid;
    if (--how_many > 0) {
        new_record_uid = xdom.data.addRecord(ref_node, target_node, target_position, how_many);
    }
    xdom.session.setData(xdom.stores.active);
    xdom.data.history.redo = [];
    if (!new_record_uid) {
        xdom.stores.active.reseed();
        var new_record = xdom.stores.active.selectSingleNode('//*[@timestamp="' + timestamp + '"]')
        var new_record_uid = new_record.getAttribute("x:id");
        xdom.data.applyTransforms(new_record);
        xdom.data.binding.trigger(new_record);
        xdom.dom.refresh();
    }
    return new_record_uid;
}

var keyInterval = undefined;
xdom.dom.controls.comboBox = {}
xdom.dom.controls.comboBox.showOptions = function (input, onkeyup) {
    if (keyInterval != undefined) {
        window.clearTimeout(keyInterval); keyInterval = undefined;
    }
    keyInterval = window.setTimeout(function () {
        if (onkeyup && onkeyup.apply) {
            var current_request = (xdom.data.binding.requests[xdom.dom.findClosestElementWithId(input).id] || {})["value"];
            if (current_request && current_request.xhr) {
                current_request.xhr.abort()
            }
            onkeyup.apply(input, [input]);
        }
    }, /*sDataSourceType == 'remote' ? 500 : */300);
}

//document.addEventListener('mousemove', function checkHover() {
//    xdom.listeners.hovered = event.srcElement;
//    //console.log((xdom.listeners.hovered || {}).tagName + ': ' + (xdom.dom.findClosestElementWithTagName(xdom.listeners.hovered, "button") || xdom.dom.findClosestElementWithClassName(xdom.listeners.hovered, "btn")))
//});

xdom.dom.controls.comboBox.onBlur = function (src) {
    if (!src) return;
    var combo = document.getElementById(src.id);
    var node_id = src.id.replace(/^_\[^_]+_/, '');
    if (combo == this) { combo.style.display = 'none'; xdom.data.update({ target: node_id, attributes: [{ '@value': (combo[combo.selectedIndex] || {}).value }] }) }
    xdom.delay(100).then(() => {
        if ((xdom.dom.findClosestElementWithId(document.activeElement) || {}).id != node_id) {
            combo.style.display = 'none';
            xdom.data.remove(xdom.stores.active.selectNodes('//@state:combo_selection'));
        }
    })

}

xdom.data.getSelections = function (data_source) {
    var data_source = (data_source || xdom.stores.active);
    return data_source.selectNodes('//*[@*[name()="x:selected"]]')
}

xdom.data.clearChecked = function (targetId) {
    var target = xdom.data.find(targetId);
    xdom.data.remove(target.selectNodes('*/@x:checked'));
}

xdom.data.clearSelection = function (targetId, refresh) {
    try {
        var oXML = xdom.xml.createDocument(xdom.stores.active);
        var oNode = oXML.selectSingleNode('/*/*[@x:selected="true"]');
        if (!oNode) return;
        oNode.setAttribute('@x:selected', null, refresh);
    } catch (e) {
        throw (e.message);
    }

}

xdom.data.unselectRecord = function (targetId, clearSelection) {
    xdom.data.update(targetId, '@x:selected', null, true);
}

xdom.data.selectRecord = function (targetId, on_complete) {
    if (!xdom.listener.keypress.ctrlKey) {
        xdom.data.removeSelections(xdom.stores.active.selectNodes('//*[@x:id="' + targetId + '"]/../*[@*[name()="x:selected"]]'));
        //xdom.data.removeSelections(xdom.stores.active.selectNodes('//*[@x:id="' + targetId + '"]/../*[@x:selected]')); //This part is buggy in IE and some times in Chrome, Edge, and probably other browsers. The workaround is adding little more verbosity in the predicate.
    }
    xdom.data.update(targetId, '@x:selected', 'true', true);
    if (on_complete && on_complete.apply) {
        on_complete.apply(this, arguments);
    };
}

xdom.data.previousRecord = function () {
    var oXML = xdom.xml.createDocument(xdom.stores.active);
    var oNode = oXML.selectSingleNode('/*/*[@x:selected="true"]');
    var oNodeNext = (oNode || {});

    do {
        var oNodeNext = oNodeNext.previousSibling;
    } while (oNodeNext && oNodeNext.nodeName == "#text")

    if (oNodeNext) {
        xdom.data.update(oNode.getAttribute("x:id"), '@x:selected', null, false);
        xdom.data.update(oNodeNext.getAttribute("x:id"), '@x:selected', 'true', true);
    }
}

xdom.data.nextRecord = function () {
    var oXML = xdom.xml.createDocument(xdom.stores.active);
    var oNode = oXML.selectSingleNode('/*/*[@x:selected="true"]');
    var oNodeNext = (oNode || {});
    do {
        var oNodeNext = oNodeNext.nextSibling;
    } while (oNodeNext && oNodeNext.nodeName == "#text")

    if (oNodeNext) {
        xdom.data.update(oNode.getAttribute("x:id"), '@x:selected', null, false);
        xdom.data.update(oNodeNext.getAttribute("x:id"), '@x:selected', 'true', true);
    }
}
//xdom.listeners.xml.onTransform = function (original, transformed) {
//    return;
//}

//xdom.listeners.xml.changingAttribute = function (node, attribute, value) {
//    //console.log('Changed = ' + attribute + ' = ' + value);
//}

//xdom.listeners.xml.onLoad = function (xdocument) {
//    return;
//}

//xdom.listeners.dom.onLoad = function (xdocument) {
//    return;
//}
var cart = {};
cart.empty = function () {
    xdom.data.remove(xdom.dom.shell.selectSingleNode('//shell:cart'));
    xdom.dom.refresh({ forced: true });
}

xdom.tools.isJSON = function (str) {
    return xdom.json.isValid(str);
}

xdom.datagrid.columns.toggleVisibility = function (column_name) {
    var transforms = xdom.data.getTransformationFileName(xdom.stores.active).split(";");
    var layout_transform = transforms.pop();
    var xsl = (xdom.library[layout_transform].document || xdom.library[layout_transform]);

    //var root_node = xsl.documentElement.selectSingleNode('//xsl:key[@name="hidden"][@match="/"]');
    //if (!root_node) {
    //    console.error((xdom.messages["datagrid.columns.toggleVisibility.error"] || "Error: datagrid.columns.toggleVisibility"));
    //    return false;
    //}
    if (column_name) {
        var key_node = xsl.documentElement.selectSingleNode('//xsl:key[@name="hidden"][@use="generate-id()"][@match="' + column_name + '"]');
        if (!key_node) {
            var new_key = xdom.xml.createDocument('<xsl:key xmlns:xsl="http://www.w3.org/1999/XSL/Transform" name="hidden" match="' + column_name + '" use="generate-id()"/>');
            //xdom.dom.insertAfter(new_key.documentElement, root_node);
            xsl.selectSingleNode('*').appendChild(new_key.documentElement);
        } else {
            xdom.data.remove(key_node);
        }
    }

    xdom.dom.refresh();
}

xdom.datagrid.columns.groupBy = function (column_name) {
    var transforms = xdom.data.getTransformationFileName(xdom.stores.active).split(";");
    var layout_transform = transforms.pop();
    var xsl = (xdom.library[layout_transform].document || xdom.library[layout_transform]);

    //var root_node = xsl.documentElement.selectSingleNode('//xsl:key[@name="groupBy"][@match="/"]');
    //if (!root_node) {
    //    console.error((xdom.messages["datagrid.columns.groupBy.error"] || "Error: datagrid.columns.groupBy"));
    //    return false;
    //}
    if (column_name) {
        var key_node = xsl.documentElement.selectSingleNode('//xsl:key[@name="groupBy"][@use="concat(name(..),\'::\',.)"][@match="' + column_name + '/@value"]');
        if (!key_node) {
            var new_key = xdom.xml.createDocument('<xsl:key xmlns:xsl="http://www.w3.org/1999/XSL/Transform" name="groupBy" match="' + column_name + '/@value" use="concat(name(..),\'::\',.)"/>');
            //xdom.dom.insertAfter(new_key.documentElement, root_node);
            xsl.selectSingleNode('*').appendChild(new_key.documentElement);
        } else {
            xdom.data.remove(key_node);
        }
    }

    xdom.dom.refresh();
}

xdom.datagrid.columns.groupCollapse = function (column_name, value) {
    var transforms = xdom.data.getTransformationFileName(xdom.stores.active).split(";");
    var layout_transform = transforms.pop();
    var xsl = (xdom.library[layout_transform].document || xdom.library[layout_transform]);

    //var root_node = xsl.documentElement.selectSingleNode('//xsl:key[@name="groupBy"][@match="/"]');
    //if (!root_node) {
    //    console.error((xdom.messages["datagrid.columns.groupCollapse.error"] || "Error: datagrid.columns.groupCollapse"));
    //    return false;
    //}
    value = value.replace(/</g, '&lt;');
    if (column_name) {
        var key_node = xsl.documentElement.selectSingleNode('//xsl:key[@name="groupCollapse"][@use="concat(name(..),\'::\',.)"][@match="' + column_name + '/@value[.=\'' + value + '\']"]');
        if (!key_node) {
            var new_key = xdom.xml.createDocument('<xsl:key xmlns:xsl="http://www.w3.org/1999/XSL/Transform" name="groupCollapse" match="' + column_name + '/@value[.=\'' + value + '\']" use="concat(name(..),\'::\',.)"/>');
            //xdom.dom.insertAfter(new_key.documentElement, root_node);
            xsl.selectSingleNode('*').appendChild(new_key.documentElement);
        } else {
            xdom.data.remove(key_node);
        }
    }

    xdom.dom.refresh();
}

xdom.data.getStylesheets = function (xml_document, predicate) {
    var predicate = (predicate ? `[${predicate}]` : '');
    var stylesheets_nodes = (xml_document || xdom.stores.active || new xdom.xml.Empty()).selectNodes("processing-instruction('xml-stylesheet')" + predicate);
    var stylesheets = [];
    for (let s = 0; s < stylesheets_nodes.length; ++s) {
        stylesheet = JSON.parse('{' + (stylesheets_nodes[s].data.match(/(\w+)=(["'])([^\2]+?)\2/ig) || []).join(", ").replace(/(\w+)=(["'])([^\2]+?)\2/ig, '"$1":$2$3$2') + '}');
        stylesheets.push(stylesheet);
    }
    return stylesheets;
}

xdom.data.filter = {};
xdom.data.filterBy = function (settings) {//filter_by, value, attribute, exclusive
    var coalesce = xdom.data.coalesce;
    var settings = (settings || {});
    //if (arguments.length > 0 && !(arguments.length == 1 && arguments[0].constructor === {}.constructor)) {
    //    var new_settings = {}
    //    new_settings["filter_by"] = arguments[0]
    //    new_settings["value"] = arguments[1]
    //    new_settings["attribute"] = arguments[2]
    //    new_settings["exclusive"] = arguments[3]
    //    new_settings["row_path"] = arguments[4]
    //    xdom.data.filterBy(new_settings);
    //    if (((arguments || {}).callee || {}).caller != xdom.data.filterBy) {
    //        xdom.dom.refresh();
    //    }
    //    return;
    //}
    var filter_by = arguments[0];//settings["filter_by"]
    if (filter_by && filter_by.constructor == [].constructor) {
        if (filter_by.length == 1) {
            filter_by = filter_by[0];
        } else {
            for (let a = filter_by.length - 1; a >= 0; --a) {
                xdom.data.filterBy(filter_by.pop())
            }
            if (((arguments || {}).callee || {}).caller != xdom.data.filterBy) {
                xdom.dom.refresh();
            }
            return;
        }
    }
    var xsl = xdom.stores.active.library[Object.values(xdom.stores.active.stylesheets.filter(el => el.role != 'init' && el.role != 'binding')).pop().href];
    var filter_node = xsl.documentElement.selectSingleNode('//xsl:key[@name="filterBy"][starts-with(@match,"*")]');
    var row_path
    if (filter_node) {
        row_path = (settings["row_path"] || filter_node.getAttribute("match").match(/^[^\[]+/)[0])
    }
    row_path = (row_path || '*');

    var filters = {};
    if (filter_by) {
        var exclusive = (arguments[1] || {}).exclusive;
        var full_path = filter_by["match"].split(/\//ig)
        //var attribute = full_path.pop();
        var root_node = full_path.join('/');//full_path.pop()

        let { use, match, attribute = "@value", value } = filter_by;
        var exclusive = coalesce(exclusive, false);

        if (exclusive) {
            let alt_filter_by = filter_by.cloneObject();
            delete alt_filter_by["value"];
            xdom.data.clearFilterOption(alt_filter_by);
        }
        let query;
        let value_definition
        if (value !== undefined) {
            use = (use || 'generate-id(self::*)');
            query = `//xsl:key[@name="filterBy"][@use="${use}"][@match="${match}/${attribute}[.=&apos;${value}&apos;]"]`
        } else {
            query = `//xsl:key[@name="filterBy"][@use="${use}"][@match="${match}/${attribute}[.=.]"]`
        }
        var filter_column = xsl.documentElement.selectSingleNode(query);
        if (!filter_column) {
            let new_key;
            if (value !== undefined) {
                new_key = xdom.xml.createDocument(`<xsl:key xmlns:xsl="http://www.w3.org/1999/XSL/Transform" name="filterBy" match="${match}/${attribute}[.=&apos;${value}&apos;]" use="${use}"/>`)
            } else {
                new_key = xdom.xml.createDocument(`<xsl:key xmlns:xsl="http://www.w3.org/1999/XSL/Transform" name="filterBy" match="${match}/${attribute}[.=.]" use="${use}"/>`)
            }
            filter_column = xsl.selectSingleNode('*').appendChild(new_key.documentElement);
        }
    }

    var filter_keys = xsl.selectNodes(`//xsl:key[@name="filterBy"][contains(@use,"::',generate-id(parent::*") or contains(@use,"::") and starts-with(@use,"'")]`);
    for (let field of filter_keys) {
        let parts = field.getAttribute("match").match(/(.*)\/([^\/]+)\/([^\/]+)\[\.=?(.*)\]$/i)
        let row_path, field_name, attr, condition_value
        if (parts) {
            [, row_path, field_name, attr, condition_value = undefined] = field.getAttribute("match").match(/(.*)\/([^\/]+)\/([^\/]+)\[\.=?(.*)\]$/i)
        } else {
            row_path = field.getAttribute("match")
        }
        condition_value = (condition_value && condition_value !== '.' && condition_value || undefined);
        filters[row_path] = (filters[row_path] || {})
        if (!field_name) {
            continue
        }
        filters[row_path][field_name] = (filters[row_path][field_name] || {})

        if (field_name == '*') {
            attr = "self::*";
        }
        if (attr) {//field.getAttribute("use") == "name()") {
            filters[row_path][field_name][attr] = (filters[row_path][field_name][attr] || []);
        }
        if (!condition_value) {//field.getAttribute("use") != "generate-id()") {
            continue;
        }
        var filter_column = xsl.selectSingleNode(`//xsl:key[@name="filterBy"][@match="${row_path}/${field_name}/${attr}[.=.]"]`)
        if (!filter_column) {
            delete field;
            continue;
        }
        filters[row_path][field_name][attr] = (filters[row_path][field_name][attr] || []);
        filters[row_path][field_name][attr].push(condition_value);//field_condition.substr(field_condition.indexOf('=') + 1));
    }

    var other_filters_nodes = xsl.documentElement.selectNodes('//xsl:key[starts-with(@name,"other_filters_")]');
    xdom.data.remove(other_filters_nodes);
    for (let row in filters) {
        for (let key in filters[row]) {
            for (let attr in filters[row][key]) {
                var other_filters = filters.cloneObject();
                delete other_filters[row][key][attr];
                var other_filters_definition = xdom.data.filter.createFilters(other_filters[row]);
                //var new_key = xdom.xml.createDocument('<xsl:key xmlns:xsl="http://www.w3.org/1999/XSL/Transform" name="other_filters_' + (key + '/' + attr).replace(/[^\d\sa-zA-Z\u00C0-\u017F]/ig, '_') + '" match="' + row_path + (other_filters_definition.length ? "[" + other_filters_definition + "]" : "") + '" use="generate-id(' + (key == '*' ? '/*' : '') + ')"/>');
                let new_key = xdom.xml.createDocument(`<xsl:key xmlns:xsl="http://www.w3.org/1999/XSL/Transform" name="other_filters_${(row + '/' + key + '/' + attr).replace(/[^\d\sa-zA-Z\u00C0-\u017F]/ig, '_')}" match="${(other_filters_definition.length ? row + '[' + other_filters_definition + ']' : "*")}" use="generate-id(${(key == '*' ? '/*' : '')})"/>`);
                xsl.selectSingleNode('*').appendChild(new_key.documentElement);
            }
        }
    }

    for (let row_path in filters) {
        var new_filters = xdom.data.filter.createFilters(filters[row_path]);
        if (!filter_node) {
            var new_key = xdom.xml.createDocument('<xsl:key xmlns:xsl="http://www.w3.org/1999/XSL/Transform" name="other_filters_' + (key + '/' + attribute).replace(/[^\d\sa-zA-Z\u00C0-\u017F]/ig, '_') + '" match="' + row_path + (new_filters.length ? "[" + new_filters + "]" : "") + '" use="generate-id(' + (key == '*' ? '/*' : '') + ')"/>'); //revisar si está bien esta nodo, porque se crea con other_fields si no existe el nodo de filtros
            xsl.selectSingleNode('*').appendChild(new_key.documentElement);
        } else {
            filter_node.setAttribute("match", row_path + (new_filters.length ? "[" + new_filters + "]" : ""));
        }
    }
    if (((arguments || {}).callee || {}).caller != xdom.data.filterBy) {
        xdom.dom.refresh();
    }
}

xdom.data.findById = function (id) {
    if (!id) { return null; }
    return xdom.stores.active.selectSingleNode('//*[@x:id="' + id + '"]')
}

xdom.data.findByName = function (name) {
    var nodes = xdom.stores.active.selectNodes('//*[name()="' + name + '"]');
    if (nodes && nodes.length == 1) {
        nodes = nodes[0];
    }
    return nodes;
}

xdom.data.coalesce = function () {
    return [...arguments].coalesce();
}

xdom.data.filter.createFilters = function (filters) {
    var filter_definition = xdom.json.join(filters, {
        separator: " and "
        , for_each: function (element, index, array) {
            if (!(element.value && Object.keys(element.value).length)) {
                array[index] = undefined;
            } else {
                var field = element.key
                var value_definition = xdom.json.join(element.value, {
                    separator: " and "
                    , for_each: function (_element, _index, _array) {
                        if (!_element.value.length) {
                            _array[_index] = undefined;
                        } else {
                            var values = []
                            for (let e = 0; e < _element.value.length; ++e) {
                                values.push(_element.key + ' = ' + _element.value[e])
                            }
                            _array[_index] = field + "[" + values.join(" or ") + "]";
                        }
                    }
                });
                if (value_definition != '') {
                    array[index] = value_definition;
                } else {
                    array[index] = undefined;
                }
            }
        }
    });
    return filter_definition;
}

xdom.data.clearFilter = function (filter_by) {
    var filter_node;
    var xsl = xdom.stores.active.library[Object.values(xdom.stores.active.stylesheets.filter(el => el.role != 'init' && el.role != 'binding')).pop().href];
    if (filter_by === undefined) {
        filter_node = xsl.documentElement.selectNodes('//xsl:key[@name="filterBy"][not(starts-with(@match,"*"))]');
    } else {
        filter_node = xsl.documentElement.selectNodes(`//xsl:key[@name="filterBy"][@use="'${filter_by}'" or @use="local-name()"]`);
    }
    xdom.data.remove(filter_node)
    xdom.data.filterBy();
}

xdom.data.clearFilterOption = function (filter_by) {
    var xsl = xdom.stores.active.library[Object.values(xdom.stores.active.stylesheets.filter(el => el.role != 'init' && el.role != 'binding')).pop().href];
    let { use = 'concat(generate-id(parent::*)~', match, attribute, value } = filter_by;
    let value_definition = '', attribute_definition = ''
    if (value !== undefined) {
        value_definition = `='${value}']`
    }
    if (attribute) {
        attribute_definition = `/${attribute}[.${value_definition}`
    }
    let filter_column;
    filter_column = xsl.documentElement.selectNodes(`//xsl:key[@name="filterBy"][contains(@use,"::',generate-id(parent::*")][starts-with(@match,"${match}${attribute_definition}")]`);
    xdom.data.remove(filter_column)
    if (((arguments || {}).callee || {}).caller != xdom.data.filterBy) {
        xdom.data.filterBy();
    }
    return;
    //var attribute = (attribute || "@value");
    //var filter_definition = "";
    //var filter_nodes;
    //var full_path = filter_by.split(/\//ig)
    //var root_node = full_path.shift();
    //var attribute = full_path.join('/');//full_path.pop()
    //if (value !== undefined) {
    //    //filter_definition = '[@match="' + root_node + "[@value='" + Encoder.HTML2Numerical(Encoder.htmlEncode(value) + "']\"]";
    //    filter_definition = '[@match="' + root_node + "[" + attribute + "='" + value + "']\"]";
    //}
    //var transforms = xdom.data.getTransformationFileName(xdom.stores.active).split(";");
    //var layout_transform = transforms.pop();
    //var xsl = xdom.stores.active.library[Object.values(xdom.stores.active.stylesheets.filter(el => el.role != 'init' && el.role != 'binding')).pop().href];
    //filter_nodes = xsl.documentElement.selectNodes('//xsl:key[@name="filterBy"][contains(concat("^",@match,"["),"^' + root_node + '[")]' + filter_definition);
    //xdom.data.remove(filter_nodes)
    //if (((arguments || {}).callee || {}).caller != xdom.data.filterBy) {
    //    xdom.data.filterBy();
    //}
}

Object.defineProperty(xdom.dom, 'shell', {
    get: function () {
        if (xdom.stores["#shell"] && !(xdom.stores["#shell"] instanceof xdom.Store)) {
            xdom.stores["#shell"] = new xdom.Store(xdom.stores["#shell"]);
        }
        xdom.dom.shell = (xdom.stores['#shell'] || ((xdom.data.default.selectSingleNode('//*[namespace-uri()="http://panax.io/shell" and local-name()="shell"]') || {}).ownerDocument || {}).store && xdom.data.default || xdom.Store('<?xml-stylesheet type="text/xsl" href="shell.xslt" role="shell" target="body"?><shell:shell xmlns:shell="http://panax.io/shell"/>'));
        return xdom.stores['#shell'];
    }
    , set: function (input) {
        if (!(input instanceof xdom.Store)) {
            input = new xdom.Store(input);
        }

        if (input) {
            if (!xdom.stores["#shell"]) {
                (xdom.manifest.getConfig("#sitemap", 'transforms') || []).filter(stylesheet => stylesheet.href && stylesheet.role == 'init').map(transform => transform = input.addStylesheet(transform));
            }
            xdom.stores["#shell"] = input;
            //xdom.dom.refresh({ forced: true });
            //xdom.stores["#shell"].render();
        }
    }
});

Object.defineProperty(xdom.dom, 'cart', {
    get: function () {
        var cart = xdom.dom.shell.selectSingleNode('//shell:cart');
        if (!cart) {
            xdom.dom.shell.selectSingleNode('//shell:shell').appendChild(xdom.xml.createDocument('<shell:cart xmlns:shell="http://panax.io/shell"></shell:cart>').documentElement);
        }
        return xdom.dom.shell.selectSingleNode('//shell:cart');
    }
});

Object.defineProperty(xdom.dom, 'settings', {
    get: function () {
        return xdom.stores.find("shell:settings")[0] || xdom.stores.defaults["#settings"];
    }
});

Object.defineProperty(xdom.dom, 'sitemap', {
    get: function () {
        return xdom.dom.shell.selectSingleNode('//*[namespace-uri()="http://panax.io/sitemap" and local-name()="sitemap"]');
    }
});

Object.defineProperty(xdom.data, 'document', {
    get: function () {
        return xdom.stores.active;
    }
    , set: async function (input) {
        xdom.stores.active = input;

    }
});
xdom.db = {};
xdom.db.Parameter = function (name, value, output) {
    Object.defineProperty(this, 'name', {
        value: name,
        writable: true, enumerable: false, configurable: false
    });
    Object.defineProperty(this, 'value', {
        value: value,
        writable: true, enumerable: false, configurable: false
    });
    Object.defineProperty(this, 'output', {
        value: output,
        writable: true, enumerable: false, configurable: false
    });
}

var pageX, curCol, nxtCol, nxtColWidth, nxtColWidth;
xdom.datagrid.columns.resize = {
    mouseover: function (e) {
        if (e.target.className.indexOf("hover") == -1) {
            e.target.className += " hover";
        } else {
            e.target.className = e.target.className.replace(" hover", "");
        }
    }
    , mousedown: function (e) {
        curCol = e.target.parentElement;
        nxtCol = curCol.nextElementSibling;
        pageX = e.pageX;

        var padding = paddingDiff(curCol);

        curColWidth = curCol.offsetWidth - padding;
        if (nxtCol)
            nxtColWidth = nxtCol.offsetWidth - padding;
        console.log("curColWidth: " + curColWidth);
        console.log("nxtColWidth: " + nxtColWidth);
    }
    , mousemove: function (e) {
        if (curCol) {
            var diffX = e.pageX - pageX;

            if (nxtCol) {
                nxtCol.style.width = (nxtColWidth - (diffX)) + 'px';
            }

            curCol.style.width = (curColWidth + diffX) + 'px';
            console.log("curCol: " + curCol.id + ': ' + (nxtColWidth - (diffX)) + 'px');
            console.log("nxtCol: " + nxtCol.id + ': ' + (curColWidth + diffX) + 'px');
        }
    }
    , mouseup: function (e) {
        curCol = undefined;
        nxtCol = undefined;
        pageX = undefined;
        nxtColWidth = undefined;
        curColWidth = undefined;
    }
}

/*deprecate ------------------------------->*/

xdom.xml.reseed = function (xml) {
    return xml.reseed()
}

xdom.json.merge = function () {
    var response = (arguments[0] || {})
    for (let a = 1; a < arguments.length; a++) {
        var object = arguments[a]
        if (object && object.constructor == {}.constructor) {
            for (let key in object) {
                if (object[key] && object[key].constructor == {}.constructor) {
                    response[key] = xdom.json.merge(response[key], object[key]);
                } else {
                    response[key] = object[key];
                }
            }
        }
    }
    return response;
}

xdom.dom.findActiveElement = function (relativePath, targetDocument) {
    var relativePath = Array.prototype.coalesce(relativePath, xdom.dom.activeElementId);
    if (!relativePath) {
        return (targetDocument || {}).body;
    }
    var targetDocument = (targetDocument || document.activeElement.contentDocument || document)
    return targetDocument.querySelector(relativePath) || xdom.dom.findActiveElement(relativePath.split(" > ").slice(0, -1).join(" > "), targetDocument);
}

xdom.dom.navigateTo = function (hashtag, public_hashtag) {
    //if (xdom.session.status != 'authorized') {
    //    return;
    //}
    hashtag = (hashtag || "").replace(/^([^#])/, '#$1');
    if ([xdom.state.seed, ...(xdom.state.activeTags() || [])].includes(hashtag) || xdom.stores[hashtag].isRendered) { //TODO: Revisar si isRendered siempre 
        xdom.state.active = hashtag;
    } else {
        xdom.state.next = hashtag;

        public_hashtag = Array.prototype.coalesce(public_hashtag || hashtag)
        var prev = (history.state["prev"] || [])
        prev.unshift(history.state.active)
        history.pushState({
            seed: hashtag
            //, active: hashtag
            , prev: prev
        }, ((event || {}).target || {}).textContent, public_hashtag);
    }
    xdom.stores.active.render();
    //window.location = window.location.href.replace(/#.*$/g, '') + xdom.state.active;
}

xdom.dom.updateHash = function (public_hashtag) {
    var current_hash = (window.top || window).location.hash;
    if (current_hash == public_hashtag) {
        return;
    }
    history.replaceState((history.state || {}), ((event || {}).target || {}).textContent, public_hashtag);
}

xdom.data.loadHash = async function (hash) {
    if (xdom.manifest.sources[hash]) return false;
    await xdom.manifest.sources[hash].fetch({ as: hash });
    if (!xdom.stores[hash]) {
        xdom.dom.navigateTo('#');
    }
}


Object.defineProperty(xdom.dom, 'current_hash', {
    get: function () {
        if (xdom.session.getKey("status") != 'authorized') {
            return "#login"
        } else {
            return xdom.state.hash/* || (window.top || window).location.hash*/ || "#";
        }
    }
});

xdom.xml.toString = function (xml) {
    if (!xml) return '';
    if (typeof xml == "string" || typeof xml == "boolean" || typeof xml == "number") {
        return xml
    } else {
        if (xml instanceof xdom.Store) {
            xml = xml.document;
        }
        return (xml.xml !== undefined ? xml.xml : new XMLSerializer().serializeToString(xml)); //(xml.documentElement || xml)
    }
}


if (!Element.prototype.hasOwnProperty('target')) {
    Object.defineProperty(Element.prototype, 'target', { /*Estaba con HTMLElement, pero los SVG los ignoraba. Se deja abierto para cualquier elemento*/
        get: function () {
            let store = this.store;
            if (!store) {
                return null;
            } else {
                let node = store.find(this.getAttribute("xo-source")) || /*store.find(this.name) || */store.find(this.id) || store.find([this.closest("[xo-scope]")].map(el => el && el.getAttribute("xo-scope") || null)[0]);
                //let attribute = (node || store.documentElement || this).getAttribute(`@${this.getAttribute("xo-attribute")}`);
                //return attribute || node || store;
                return node;
            }
        }
    });
}
if (!HTMLElement.prototype.hasOwnProperty('sourceNode')) { /*Deprecar para la versión final*/
    Object.defineProperty(Element.prototype, 'sourceNode', { /*Estaba con HTMLElement, pero los SVG los ignoraba. Se deja abierto para cualquier elemento*/
        get: function () {
            let store = this.store;
            if (!store) {
                return null;
            } else {
                let node = store.find(this.getAttribute("xo-source")) || /*store.find(this.name) || */store.find(this.id) || store.find([this.closest("[xo-scope]")].map(el => el && el.getAttribute("xo-scope") || null)[0]);
                //let attribute = (node || store.documentElement || this).getAttribute(`@${this.getAttribute("xo-attribute")}`);
                //return attribute || node || store;
                return node;
            }
        }
    });
}

xdom.xml.lookupNamespaceURI = function (node, namespace_uri) {
    if (!(node && typeof (node.selectSingleNode) != "undefined")) {
        return;
    }
    return node.getElementsByTagNameNS(namespace_uri, '*');
    //return node.selectSingleNode("namespace::*[.='" + namespace_uri + "']");
}
xdom.xml.createNSResolver = function () {
    var namespaces = element.ownerDocument.createNSResolver(element)//xdom.xml.getNamespaces.apply(this, arguments);
    return function (prefix) {
        return (namespaces[prefix] || xdom.xml.namespaces[prefix]) || null;
    };
}

xdom.data.self = {}

xdom.data.self = function () {
    var uid = event.srcElement.id;
    var _self = xdom.stores.active.selectSingleNode('//*[@x:id="' + uid + '"]')
    _self.remove = function (str) {
        nodes = _self.selectNodes(str);
        xdom.data.remove(nodes);
        xdom.dom.refresh();
    }
    return _self;
}

xdom.data.getNode = function (uid) {
    return xdom.stores.active.selectSingleNode("//*[@x:id='" + uid + "']/ancestor-or-self::*[namespace-uri()!='http://panax.io/xdom'][1]");
}

xdom.data.getCurrent = function () {
    var uid = event.srcElement.id;
    return xdom.stores.active.selectSingleNode("//*[@x:id='" + uid + "']/ancestor-or-self::*[namespace-uri()!='http://panax.io/xdom'][1]");
}

xdom.data.replace = function (target, replace_by) {
    target.parentNode.replaceChild(replace_by, target);
}

xdom.data.remove = function (target, data_source) {
    data_source = (data_source || xdom.stores.active)
    if (!target) return;
    xdom.data.history.saveState();
    if (typeof (target) == "string") {
        target = xdom.data.deepFind(target);
    }
    if (!target) {
        return undefined;
    }
    if (target.length !== undefined) {
        for (let node = 0; node < target.length; ++node) {
            xdom.data.remove(target[node]);
        }
    } else {
        if (target instanceof Element) {
            target.remove();
        } else if (target.nodeType == 2/*attribute*/) {
            var attribute_name = target.nodeName;
            var ownerElement = (target.ownerElement || target.selectSingleNode('..'))
            ownerElement.removeAttribute(attribute_name);
        } else {
            target.parentNode.removeChild(target);
        }
    }
    if (!(((arguments || {}).callee || {}).caller)) {
        xdom.dom.refresh();
    }
    return !target;
}

xdom.data.removeProcessingInstructions = function (data) {
    var xsl = xdom.xml.createDocument('                                                                                 \
                <xsl:stylesheet version="1.0"                                                                           \
                    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"                                                    \
                    xmlns="http://www.w3.org/1999/xhtml">                                                               \
                    <xsl:output method="xml" indent="no" />                                                             \
                    <xsl:template match="processing-instruction()"/>                                                    \
                    <xsl:template match="node()">                                                                       \
                    <xsl:copy-of select="."/>                                                                           \
                    </xsl:template>                                                                                     \
                </xsl:stylesheet> ');
    data = xdom.xml.transform(data, xsl);
    return data;
}

xdom.xml.appendChild = function (target, child) {
    var namespaces = xdom.xml.createNamespaceDeclaration(target.ownerDocument);
    var xsl_transform = xdom.xml.createDocument('\
<xsl:stylesheet version="1.0" \
     xmlns:xsl="http://www.w3.org/1999/XSL/Transform"\
     '+ namespaces + '>                     \
      <xsl:output method="xml" indent="no"/>\
      <xsl:template match="@* | node() | text()">\
        <xsl:copy>\
          <xsl:apply-templates select="@*"/>\
          <xsl:apply-templates select="node() | text()"/>\
        </xsl:copy>\
      </xsl:template>\
    <xsl:template match="/*">              \
        <xsl:copy >                                    \
        <xsl:apply-templates select="@*" />         '+
        xdom.xml.toString(child)
        + '    <xsl:apply-templates select="node()|text()" />\
        </xsl:copy >                                        \
  </xsl:template >                                      \
    </xsl:stylesheet>');
    return xdom.xml.transform(target, xsl_transform);
}

xdom.data.setAttribute = function (nodes, attribute, value) {
    nodes.setAttribute(attribute.substring(1), value);
}

xdom.data.removeAttribute = function (nodes, attribute) {
    for (let node = 0; node < nodes.length; ++node) {
        nodes[node].removeAttribute(attribute.substring(1));
    }
}

xdom.storage.getData = function () {
    if (typeof (Storage) !== "undefined") {
        var document = localStorage.getItem(location.pathname.replace(/[^/]+$/, "") + "xdom.data")
        if (document) {
            xdom.stores.active = xdom.xml.createDocument(document);
        }
    } else {
        console.error('Storage is not supported by your browser')
    }
}

xdom.storage.getDocument = function (key) {
    //if (!eval(xdom.storage.enabled) && key != 'xdom.storage.enabled') return;
    if (typeof (Storage) !== "undefined") {
        var document = localStorage.getItem(location.pathname.replace(/[^/]+$/, "") + key);
        if (document) {
            return xdom.xml.createDocument(document);
        }
    } else {
        console.error('Storage is not supported by your browser')
    }
}

//window.addEventListener("beforeprint", function (event) {
//    event.preventDefault();
//    event.returnValue = "";
//    xdom.dom.print();
//});

//if ('matchMedia' in window) {
//    window.matchMedia('print').addListener(function (media) {
//        xdom.dom.print()
//    });
//} else {
//    window.onbeforeprint = function () {
//        xdom.dom.print()
//    }
//}

/**
 * A Javascript object to encode and/or decode html characters
 * @Author R Reid
 * source: http://www.strictly-software.com/htmlencode
 * Licence: GPL
 * 
 * Revision:
 *  2011-07-14, Jacques-Yves Bleau: 
 *       - fixed conversion error with capitalized accentuated characters
 *       + converted arr1 and arr2 to object property to remove redundancy
 */

Encoder = {

    // When encoding do we convert characters into html or numerical entities
    EncodeType: "entity",  // entity OR numerical

    isEmpty: function (val) {
        if (val) {
            return ((val === null) || val.length == 0 || /^\s+$/.test(val));
        } else {
            return true;
        }
    },
    arr1: new Array('&nbsp;', '&iexcl;', '&cent;', '&pound;', '&curren;', '&yen;', '&brvbar;', '&sect;', '&uml;', '&copy;', '&ordf;', '&laquo;', '&not;', '&shy;', '&reg;', '&macr;', '&deg;', '&plusmn;', '&sup2;', '&sup3;', '&acute;', '&micro;', '&para;', '&middot;', '&cedil;', '&sup1;', '&ordm;', '&raquo;', '&frac14;', '&frac12;', '&frac34;', '&iquest;', '&Agrave;', '&Aacute;', '&Acirc;', '&Atilde;', '&Auml;', '&Aring;', '&Aelig;', '&Ccedil;', '&Egrave;', '&Eacute;', '&Ecirc;', '&Euml;', '&Igrave;', '&Iacute;', '&Icirc;', '&Iuml;', '&ETH;', '&Ntilde;', '&Ograve;', '&Oacute;', '&Ocirc;', '&Otilde;', '&Ouml;', '&times;', '&Oslash;', '&Ugrave;', '&Uacute;', '&Ucirc;', '&Uuml;', '&Yacute;', '&THORN;', '&szlig;', '&agrave;', '&aacute;', '&acirc;', '&atilde;', '&auml;', '&aring;', '&aelig;', '&ccedil;', '&egrave;', '&eacute;', '&ecirc;', '&euml;', '&igrave;', '&iacute;', '&icirc;', '&iuml;', '&eth;', '&ntilde;', '&ograve;', '&oacute;', '&ocirc;', '&otilde;', '&ouml;', '&divide;', '&Oslash;', '&ugrave;', '&uacute;', '&ucirc;', '&uuml;', '&yacute;', '&thorn;', '&yuml;', '&quot;', '&amp;', '&lt;', '&gt;', '&oelig;', '&oelig;', '&scaron;', '&scaron;', '&yuml;', '&circ;', '&tilde;', '&ensp;', '&emsp;', '&thinsp;', '&zwnj;', '&zwj;', '&lrm;', '&rlm;', '&ndash;', '&mdash;', '&lsquo;', '&rsquo;', '&sbquo;', '&ldquo;', '&rdquo;', '&bdquo;', '&dagger;', '&dagger;', '&permil;', '&lsaquo;', '&rsaquo;', '&euro;', '&fnof;', '&alpha;', '&beta;', '&gamma;', '&delta;', '&epsilon;', '&zeta;', '&eta;', '&theta;', '&iota;', '&kappa;', '&lambda;', '&mu;', '&nu;', '&xi;', '&omicron;', '&pi;', '&rho;', '&sigma;', '&tau;', '&upsilon;', '&phi;', '&chi;', '&psi;', '&omega;', '&alpha;', '&beta;', '&gamma;', '&delta;', '&epsilon;', '&zeta;', '&eta;', '&theta;', '&iota;', '&kappa;', '&lambda;', '&mu;', '&nu;', '&xi;', '&omicron;', '&pi;', '&rho;', '&sigmaf;', '&sigma;', '&tau;', '&upsilon;', '&phi;', '&chi;', '&psi;', '&omega;', '&thetasym;', '&upsih;', '&piv;', '&bull;', '&hellip;', '&prime;', '&prime;', '&oline;', '&frasl;', '&weierp;', '&image;', '&real;', '&trade;', '&alefsym;', '&larr;', '&uarr;', '&rarr;', '&darr;', '&harr;', '&crarr;', '&larr;', '&uarr;', '&rarr;', '&darr;', '&harr;', '&forall;', '&part;', '&exist;', '&empty;', '&nabla;', '&isin;', '&notin;', '&ni;', '&prod;', '&sum;', '&minus;', '&lowast;', '&radic;', '&prop;', '&infin;', '&ang;', '&and;', '&or;', '&cap;', '&cup;', '&int;', '&there4;', '&sim;', '&cong;', '&asymp;', '&ne;', '&equiv;', '&le;', '&ge;', '&sub;', '&sup;', '&nsub;', '&sube;', '&supe;', '&oplus;', '&otimes;', '&perp;', '&sdot;', '&lceil;', '&rceil;', '&lfloor;', '&rfloor;', '&lang;', '&rang;', '&loz;', '&spades;', '&clubs;', '&hearts;', '&diams;'),
    arr2: new Array('&#160;', '&#161;', '&#162;', '&#163;', '&#164;', '&#165;', '&#166;', '&#167;', '&#168;', '&#169;', '&#170;', '&#171;', '&#172;', '&#173;', '&#174;', '&#175;', '&#176;', '&#177;', '&#178;', '&#179;', '&#180;', '&#181;', '&#182;', '&#183;', '&#184;', '&#185;', '&#186;', '&#187;', '&#188;', '&#189;', '&#190;', '&#191;', '&#192;', '&#193;', '&#194;', '&#195;', '&#196;', '&#197;', '&#198;', '&#199;', '&#200;', '&#201;', '&#202;', '&#203;', '&#204;', '&#205;', '&#206;', '&#207;', '&#208;', '&#209;', '&#210;', '&#211;', '&#212;', '&#213;', '&#214;', '&#215;', '&#216;', '&#217;', '&#218;', '&#219;', '&#220;', '&#221;', '&#222;', '&#223;', '&#224;', '&#225;', '&#226;', '&#227;', '&#228;', '&#229;', '&#230;', '&#231;', '&#232;', '&#233;', '&#234;', '&#235;', '&#236;', '&#237;', '&#238;', '&#239;', '&#240;', '&#241;', '&#242;', '&#243;', '&#244;', '&#245;', '&#246;', '&#247;', '&#248;', '&#249;', '&#250;', '&#251;', '&#252;', '&#253;', '&#254;', '&#255;', '&#34;', '&#38;', '&#60;', '&#62;', '&#338;', '&#339;', '&#352;', '&#353;', '&#376;', '&#710;', '&#732;', '&#8194;', '&#8195;', '&#8201;', '&#8204;', '&#8205;', '&#8206;', '&#8207;', '&#8211;', '&#8212;', '&#8216;', '&#8217;', '&#8218;', '&#8220;', '&#8221;', '&#8222;', '&#8224;', '&#8225;', '&#8240;', '&#8249;', '&#8250;', '&#8364;', '&#402;', '&#913;', '&#914;', '&#915;', '&#916;', '&#917;', '&#918;', '&#919;', '&#920;', '&#921;', '&#922;', '&#923;', '&#924;', '&#925;', '&#926;', '&#927;', '&#928;', '&#929;', '&#931;', '&#932;', '&#933;', '&#934;', '&#935;', '&#936;', '&#937;', '&#945;', '&#946;', '&#947;', '&#948;', '&#949;', '&#950;', '&#951;', '&#952;', '&#953;', '&#954;', '&#955;', '&#956;', '&#957;', '&#958;', '&#959;', '&#960;', '&#961;', '&#962;', '&#963;', '&#964;', '&#965;', '&#966;', '&#967;', '&#968;', '&#969;', '&#977;', '&#978;', '&#982;', '&#8226;', '&#8230;', '&#8242;', '&#8243;', '&#8254;', '&#8260;', '&#8472;', '&#8465;', '&#8476;', '&#8482;', '&#8501;', '&#8592;', '&#8593;', '&#8594;', '&#8595;', '&#8596;', '&#8629;', '&#8656;', '&#8657;', '&#8658;', '&#8659;', '&#8660;', '&#8704;', '&#8706;', '&#8707;', '&#8709;', '&#8711;', '&#8712;', '&#8713;', '&#8715;', '&#8719;', '&#8721;', '&#8722;', '&#8727;', '&#8730;', '&#8733;', '&#8734;', '&#8736;', '&#8743;', '&#8744;', '&#8745;', '&#8746;', '&#8747;', '&#8756;', '&#8764;', '&#8773;', '&#8776;', '&#8800;', '&#8801;', '&#8804;', '&#8805;', '&#8834;', '&#8835;', '&#8836;', '&#8838;', '&#8839;', '&#8853;', '&#8855;', '&#8869;', '&#8901;', '&#8968;', '&#8969;', '&#8970;', '&#8971;', '&#9001;', '&#9002;', '&#9674;', '&#9824;', '&#9827;', '&#9829;', '&#9830;'),

    // Convert HTML entities into numerical entities
    HTML2Numerical: function (s) {
        return this.swapArrayVals(s, this.arr1, this.arr2);
    },

    // Convert Numerical entities into HTML entities
    NumericalToHTML: function (s) {
        return this.swapArrayVals(s, this.arr2, this.arr1);
    },


    // Numerically encodes all unicode characters
    numEncode: function (s) {

        if (this.isEmpty(s)) return "";

        var e = "";
        for (let i = 0; i < s.length; i++) {
            var c = s.charAt(i);
            if (c < " " || c > "~") {
                c = "&#" + c.charCodeAt() + ";";
            }
            e += c;
        }
        return e;
    },

    // HTML Decode numerical and HTML entities back to original values
    htmlDecode: function (s) {

        var c, m, d = s;

        if (this.isEmpty(d)) return "";

        // convert HTML entites back to numerical entites first
        d = this.HTML2Numerical(d);

        // look for numerical entities &#34;
        arr = d.match(/&#[0-9]{1,5};/g);

        // if no matches found in string then skip
        if (arr != null) {
            for (let x = 0; x < arr.length; x++) {
                m = arr[x];
                c = m.substring(2, m.length - 1); //get numeric part which is refernce to unicode character
                // if its a valid number we can decode
                if (c >= -32768 && c <= 65535) {
                    // decode every single match within string
                    d = d.replace(m, String.fromCharCode(c));
                } else {
                    d = d.replace(m, ""); //invalid so replace with nada
                }
            }
        }

        return d;
    },

    // encode an input string into either numerical or HTML entities
    htmlEncode: function (s, dbl) {

        if (this.isEmpty(s)) return "";

        // do we allow double encoding? E.g will &amp; be turned into &amp;amp;
        dbl = dbl || false; //default to prevent double encoding

        // if allowing double encoding we do ampersands first
        if (dbl) {
            if (this.EncodeType == "numerical") {
                s = s.replace(/&/g, "&#38;");
            } else {
                s = s.replace(/&/g, "&amp;");
            }
        }

        // convert the xss chars to numerical entities ' " < >
        s = this.XSSEncode(s, false);

        if (this.EncodeType == "numerical" || !dbl) {
            // Now call function that will convert any HTML entities to numerical codes
            s = this.HTML2Numerical(s);
        }

        // Now encode all chars above 127 e.g unicode
        s = this.numEncode(s);

        // now we know anything that needs to be encoded has been converted to numerical entities we
        // can encode any ampersands & that are not part of encoded entities
        // to handle the fact that I need to do a negative check and handle multiple ampersands &&&
        // I am going to use a placeholder

        // if we don't want double encoded entities we ignore the & in existing entities
        if (!dbl) {
            s = s.replace(/&#/g, "##AMPHASH##");

            if (this.EncodeType == "numerical") {
                s = s.replace(/&/g, "&#38;");
            } else {
                s = s.replace(/&/g, "&amp;");
            }

            s = s.replace(/##AMPHASH##/g, "&#");
        }

        // replace any malformed entities
        s = s.replace(/&#\d*([^\d;]|$)/g, "$1");

        if (!dbl) {
            // safety check to correct any double encoded &amp;
            s = this.correctEncoding(s);
        }

        // now do we need to convert our numerical encoded string into entities
        if (this.EncodeType == "entity") {
            s = this.NumericalToHTML(s);
        }

        return s;
    },

    // Encodes the basic 4 characters used to malform HTML in XSS hacks
    XSSEncode: function (s, en) {
        if (!this.isEmpty(s)) {
            en = en || true;
            // do we convert to numerical or html entity?
            if (en) {
                s = s.replace(/\'/g, "&#39;"); //no HTML equivalent as &apos is not cross browser supported
                s = s.replace(/"/g, "&quot;");
                s = s.replace(/</g, "&lt;");
                s = s.replace(/>/g, "&gt;");
            } else {
                s = s.replace(/\'/g, "&#39;"); //no HTML equivalent as &apos is not cross browser supported
                s = s.replace(/"/g, "&#34;");
                s = s.replace(/</g, "&#60;");
                s = s.replace(/>/g, "&#62;");
            }
            return s;
        } else {
            return "";
        }
    },

    // returns true if a string contains html or numerical encoded entities
    hasEncoded: function (s) {
        if (/&#[0-9]{1,5};/g.test(s)) {
            return true;
        } else if (/&[A-Z]{2,6};/gi.test(s)) {
            return true;
        } else {
            return false;
        }
    },

    // will remove any unicode characters
    stripUnicode: function (s) {
        return s.replace(/[^\x20-\x7E]/g, "");

    },

    // corrects any double encoded &amp; entities e.g &amp;amp;
    correctEncoding: function (s) {
        return s.replace(/(&amp;)(amp;)+/, "$1");
    },


    // Function to loop through an array swaping each item with the value from another array e.g swap HTML entities with Numericals
    swapArrayVals: function (s, arr1, arr2) {
        if (this.isEmpty(s)) return "";
        var re;
        if (arr1 && arr2) {
            //ShowDebug("in swapArrayVals arr1.length = " + arr1.length + " arr2.length = " + arr2.length)
            // array lengths must match
            if (arr1.length == arr2.length) {
                for (let x = 0, i = arr1.length; x < i; x++) {
                    re = new RegExp(arr1[x], 'g');
                    s = s.replace(re, arr2[x]); //swap arr1 item with matching item from arr2	
                }
            }
        }
        return s;
    },

    inArray: function (item, arr) {
        for (let i = 0, x = arr.length; i < x; i++) {
            if (arr[i] === item) {
                return i;
            }
        }
        return -1;
    }

    // Extended by Uriel Gómez	
    , urlEncode: function (url) {
        return Encoder.swapArrayVals(escape(url), ['\/', '\@', '\\\+'], ['%2F', '%40', '%2B'])
    }
}

xdom.fetch.Response = function (xhr) {
    if (!(this instanceof xdom.fetch.Response)) return new xdom.fetch.Response(xhr);
    //TODO: Que el usuario pueda activar o desactivar el "escape"
    if (xhr.responseXML && xhr.responseXML.firstElementChild.namespaceURI != 'http://www.w3.org/1999/XSL/Transform' && xhr.responseText.replace(/(\>|^)[\r\n\s]+(\<|$)/ig, "$1$2").match(/\r\n/)) { /*xhr.responseXML no escapa saltos de línea cuando vienen en atributos, por eso se reemplazan por la entidad.*/
        this.responseText = xhr.responseText.replace(/(\>|^)[\r\n\s]+(\<|$)/ig, "$1$2").replace(/\r\n/ig, "&#10;");
        this.responseXML = xdom.xml.createDocument(this.responseText);
    } else {
        this.responseText = xhr.responseText;
        this.responseXML = xhr.responseXML;
    }
    var value;
    Object.defineProperty(this, 'headers', {
        get: function get() {
            var _all_headers = xhr.getAllResponseHeaders().split(/[\r\n]+/);
            _headers = {}
            for (let h in _all_headers) {
                var header = _all_headers[h].split(/\s*:\s*/);
                if (!(header.length > 1)) {
                    continue;
                }
                _headers[header.shift().toLowerCase()] = header.join(":");
            }
            return _headers;
        }
    });
    Object.defineProperty(this, 'status', {
        get: function get() {
            return xhr.status;
        }
    });
    Object.defineProperty(this, 'document', {
        get: function get() {
            switch (this.type) {
                case "xml":
                case "html":
                    return xdom.xml.createDocument(this.responseXML || this.responseText);
                    break;
                default:
                    return this.responseText;
            }
        }
    });
    Object.defineProperty(this, 'value', {
        get: function get() {
            switch (this.type) {
                case "json":
                    var json
                    try {
                        json = JSON.parse(this.responseText);
                    } catch (e) {
                        json = eval("(${ this.responseText })");
                    }
                    return json
                    break;
                case "xml":
                case "html":
                    return this.document;
                    break;
                case "script":
                    try {
                        var return_value;
                        eval('return_value = new function(){' + this.responseText + '\nreturn this;}()');
                        return return_value;
                    } catch (e) {
                        return null;
                    }
                default:
            }
        }
    });
    Object.defineProperty(this, 'type', {
        get: function get() {
            if ((this.headers["content-type"] || "").indexOf("json") != -1 || xdom.json.isValid(this.responseText)) {
                return "json";
            } else if ((this.headers["content-type"] || "").indexOf("xml") != -1 || this.responseXML && this.responseXML.documentElement || this.responseText.indexOf("<?xml ") >= 0) {
                return "xml"
            } else {
                if (this.responseText.toUpperCase().indexOf("<HTML") != -1) {
                    return "html";
                } else {
                    return "script";
                }
            }
        }
    });
    Object.defineProperty(this, 'contentType', {
        get: function get() {
            return this.headers["content-type"].split(";")[0];
        }
    });
    Object.defineProperty(this, 'charset', {
        get: function get() {
            return this.headers["content-type"].split(";")[1];
        }
    });
    switch (this.type) {
        case "json":
            Object.defineProperty(this, 'json', {
                get: function get() {
                    var json
                    try {
                        json = JSON.parse(this.responseText);
                    } catch (e) {
                        json = eval("(" + this.responseText + ")");
                    }
                    return json;
                }
            });
            break;
        case "script":
            Object.defineProperty(this, 'object', {
                get: function get() {
                    try {
                        var return_value;
                        eval('return_value = new function(){' + this.responseText + '\nreturn this;}()');
                        return return_value;
                    } catch (e) {
                        return null;
                    }
                }
            });
        default:
    }
    return this;
}

xdom.updateData = function (xdoc) {
    if (!xdoc) {
        throw ("Data can't be set to null");
        return false;
    }
    xdom.stores.active = xdoc
    return true
}

xdom.data.selectNode = function (uid) {
    xdom.data.removeSelections(xdom.stores.active.selectNodes('//*[@x:id="' + uid + '"]/..//*[@x:selected]'));
    xdom.data.setAttribute(xdom.stores.active.selectNodes('//*[@x:id="' + uid + '"]|//*[@x:id="' + uid + '"]//*[count(preceding-sibling::*)=0 and count(following-sibling::*[@x:selected])=0]'), '@x:selected', 'true');
    xdom.stores.active = xdom.xml.createDocument(xdom.stores.active);
    xdom.dom.refresh();
}

xdom.dom.appendFirst = function (e, i) {
    if (!(e && i)) {
        return;
    }
    e = (e.documentElement || e)
    e.insertBefore((i.documentElement || i), e.firstElementChild);
}

xdom.dom.insertAfter = function (i, e, p) {
    if (e && e.nextElementSibling) {
        e.parentNode.insertBefore(i, e.nextElementSibling);
    } else {
        (p || (e || {}).parentNode).appendChild(i);
    }
}

xdom.dom.insertBefore = function (i, e, p) {
    if (e) {
        e.parentNode.insertBefore(i, e);
    } else {
        (p || (e || {}).parentNode).appendChild(i);
    }
}

xdom.dom.findClosestDataNode = function (element) {
    if (!element) return element;
    return (xdom.stores.find((element.id || '').replace(/^container_/i, ''))[0] || xdom.dom.findClosestDataNode(xdom.dom.findClosestElementWithId(element.parentElement)));
}

xdom.dom.appendChild = function (target, sHTML) {
    if (!sHTML) return;
    if (typeof (target) == "string") {
        target = document.querySelector(target);
    }
    try {
        target.appendChild(sHTML);
    } catch (e) {
        try {
            target.appendChild(xdom.xml.createDocument(sHTML).documentElement);
        } catch (e) {
            var div = document.createElement('div');
            div.innerHTML = xdom.xml.toString(sHTML);
            target.appendChild(div.children[0]);
        }
    }
}

xdom.dom.state = new Proxy({}, {
    get: function (target, name) {
        return target[name] || xdom.session.getKey("state:" + name);
    },
    set: function (target, name, value) {
        let refresh;
        if (value && ['object', 'function'].includes(typeof (value))) {
            throw ('State value is not valid type');
        }
        if (target[name] != value) {
            refresh = true
        }
        xdom.session.setKey("state:" + name, value);
        var return_value;
        if (refresh) {
            var name = name, value = value;

            let libraries = Object.values(xdom.stores).reduce((libraries, document) => {
                libraries = [...libraries, ...Object.values(document.library || {})];
                return libraries;
            }, []);
            Promise.all(libraries).then(() => {
                Object.values(xdom.stores.getActive()).filter(document => {
                    return [...Object.values(document.library || {})].filter(stylesheet => {
                        return !!(stylesheet || window.document.createElement('p')).selectFirst(`//xsl:stylesheet/xsl:param[@name='state:${name}']`)
                    }).length
                }).map(document => {
                    console.log(`Rendering ${document.tag} triggered by ${name}`);
                    return_value = document.render(true, { delay: 0 });
                });
            });
        }
        return (return_value || Promise.resolve());
    }
});

Object.defineProperty(xdom.session, 'setUserLogin', {
    value: function (user_login, data) {
        data = (data || xdom.stores.active);
        xdom.session.setKey("userLogin", user_login);
        if (data) {
            data = xdom.xml.transform(data, "xdom/normalize_namespaces.xslt");
            if ((user_login == data.selectSingleNode('//*[@session:status][1]/@session:user_login') || {}).value) {
                return;
            }
        }
    },
    writable: false, enumerable: false, configurable: false
});

Object.defineProperty(xdom.session, 'setUserId', {
    value: function (user_id, data) {
        data = (data || xdom.stores.active);
        xdom.session.setKey("userId", user_id);
        if (data) {
            data = xdom.xml.transform(data, "xdom/normalize_namespaces.xslt");
            if ((user_id == data.selectSingleNode('//*[@session:status][1]/@session:user_id') || {}).value) {
                return;
            }

            //xdom.data.update({
            //    "target": data.selectSingleNode("/*[1]")
            //    , "attributes": [
            //        { "@session:user_id": (user_id || "") }
            //        , { "@session:status": user_id ? 'authorized' : ((data.selectSingleNode("//*[@session:status='authorizing']/@session:status") || {}).value || 'unauthorized') }
            //    ]
            //});
        }
    },
    writable: false, enumerable: false, configurable: false
});

xdom.deprecated.showShell = function () {
    var current_hash = (window.top || window).location.hash;
    var target;
    if (!xdom.stores["#shell"]) {
        if (xdom.sources["#sitemap"] && ((arguments || {}).callee || {}).caller !== xdom.dom.showShell) {
            xdom.data.load(xdom.sources["#sitemap"], function () {
                xdom.dom.showShell();
            });
            return;
        } else {
            console.warn(xdom.messages["noSitemap"] || "There is no sitemap in memory or a source definition for xdom.sources.sitemap.")
        }
        return;
    }
    if (current_hash != '#shell' && xdom.stores["#shell"]) {
        xdom.stores.active = xdom.stores["#shell"];
        xdom.dom.refresh();
        target = document.querySelector('main');
    }

    if (xdom.stores[current_hash]) {
        xdom.stores.active = (xdom.stores[current_hash] || xdom.stores.active || xdom.library["default.xml"]);
        xdom.dom.refresh(target);
        return;
    }
}

xdom.devTools = {};
xdom.devTools.debug = function (enabled) {
    xdom.debug = xdom.json.merge(xdom.debug, { enabled: xdom.data.coalesce(enabled, true) })
    xdom.session.setAttribute("session:debug", xdom.debug.enabled);
    return;
}

Object.defineProperty(xdom.session, 'setAttribute', {
    value: function (attribute, value) {
        xdom.library["xdom/resources/session.xslt"].selectNodes(`(//xsl:copy/xsl:attribute[@name="${attribute}" and text()!="${String(value).replace(/\"/g, '&quote;')}"])`).remove();
        if (value != undefined) {
            xdom.library["xdom/resources/session.xslt"].selectAll(`//xsl:template[@match="/*"]/xsl:copy[not(xsl:attribute[@name="${attribute}"])]/xsl:copy-of[@select="@*"]`).map(node => node.appendAfter(xdom.xml.createDocument(`<xsl:attribute xmlns:xsl="http://www.w3.org/1999/XSL/Transform" name="${attribute}">${value}</xsl:attribute>`)))
        }
    },
    writable: false, enumerable: false, configurable: false
});

Object.defineProperty(xdom.session, 'getSessionFile', {
    value: async function () {
        var session_xsl = xdom.library["xdom/resources/session.xslt"];
        if (!session_xsl) {
            var url = "xdom/resources/session.xslt";
            let [data, request] = await xdom.fetch('xdom/resources/session.xslt').then(response => [response.body, response.request]);
            //var pathname = [...(location.pathname.match(/[^/]+/ig) || [])].filter((el, index, array) => !(array.length - 1 == index && el.indexOf('.') != -1)).join('/');
            //xdom.library[request.url.replace(new RegExp(`^${location.origin}/${pathname}`), "").replace(/^\/+/, '')] = xdom.xml.createDocument(data)
            xdom.library[request.url.replace(new RegExp(`^${location.origin}`), "").replace(new RegExp(`^${location.pathname.replace(/[^/]+$/, "")}`), "").replace(/^\/+/, '')] = xdom.xml.createDocument(data)
            session_xsl = xdom.library["xdom/resources/session.xslt"];
        }
        if (!(xdom.manifest.server || {}).endpoints["login"] && session_xsl.selectFirst(`//xsl:processing-instruction/text()[contains(.,'role="login"')]/..`)) {
            (session_xsl.selectFirst(`//xsl:processing-instruction/text()[contains(.,'role="login"')]/..`) || document.createElement('p')).remove();
        }
        return session_xsl;
    },
    writable: false, enumerable: false, configurable: false
});

xdom.data.history = {};
xdom.data.history.cascade = {};
xdom.data.history.undo = [];
xdom.data.history.redo = [];

xdom.data.history.saveState = function () {
    xdom.data.history.undo.push(xdom.xml.createDocument(xdom.stores.active));
    xdom.data.history.redo = [];
}

xdom.data.Binding = function (node) {
    if (!(this instanceof xdom.data.Binding)) return new xdom.data.Binding();
    Object.defineProperty(this, 'id', {
        value: node.getAttribute("x:id"),
        writable: false, enumerable: false, configurable: false
    });
    Object.defineProperty(this, 'node', {
        get: function () {
            return xdom.xml.createDocument(xdom.stores.active).selectSingleNode('//*[@x:id="' + this.id + '"]');
        }
    });
    Object.defineProperty(this, 'nodeName', {
        get: function () {
            return this.node.nodeName
        }
        , enumerable: false, configurable: false
    });
    Object.defineProperty(this, 'value', {
        get: function () {
            return this.node.getAttribute('value')
        }
        , enumerable: false, configurable: false
    });
    Object.defineProperty(this, 'text', {
        get: function () {
            return this.node.getAttribute('text')
        }
        , enumerable: false, configurable: false
    });
    Object.defineProperty(this, 'dependants', {
        value: {},
        writable: true, enumerable: false, configurable: false
    });

    Object.defineProperty(this, 'formulas', {
        value: {},
        writable: true, enumerable: false, configurable: false
    });
    this.updated = true;
    return this;
}

xdom.data.Dependency = function () {
    if (!(this instanceof xdom.data.Dependency)) return new xdom.data.Dependency();
    Object.defineProperty(this, 'attributes', {
        value: {},
        writable: true, enumerable: false, configurable: false
    });
    Object.defineProperty(this, 'sources', {
        value: {},
        writable: true, enumerable: false, configurable: false
    });
    this.updated = false;
    return this;
}

xdom.data.processDocument = async function (document) {
    if (!document.documentElement) {
        return;
    }
    if (!(document instanceof xdom.Store)) {
        document = xdom.Store(document);
    }
    document.reseed();
    if (document.selectSingleNode('//*[namespace-uri()="http://panax.io/shell" and local-name()="shell"]|//*[namespace-uri()="http://panax.io/sitemap" and local-name()="sitemap"]')) {
        if (!xdom.dom.shell.documentElement) {
            xdom.dom.shell = document
        } else {
            Object.entries(xdom.json.difference(xdom.xml.getNamespaces(document), xdom.xml.getNamespaces(xdom.dom.shell))).map(entry => xdom.dom.shell.documentElement.setAttribute(entry[0], entry[1]), false);
            document.selectNodes('/*/@*[name()!="x:id"]').map(attr => xdom.dom.shell.documentElement.setAttribute(attr.name, attr.value), false);
            var shell = xdom.dom.shell;
            //shell.documentElement.setAttribute("state:refresh", "true");
            if (shell.stylesheets.length == 0) {
                var stylesheets = document.selectNodes("processing-instruction('xml-stylesheet')");
                if (stylesheets.length == 0) {
                    var pi = shell.createProcessingInstruction('xml-stylesheet', 'type="text/xsl" href="shell.xslt" role="shell" target="body"');
                    shell.insertBefore(pi, shell.firstChild);
                } else {
                    for (let s = 0; s < stylesheets.length; ++s) {
                        var pi = shell.createProcessingInstruction('xml-stylesheet', stylesheets[s].data);
                        shell.insertBefore(pi, shell.firstChild);
                    }
                }
            }

            var nodes = document.selectNodes('//*[namespace-uri()="http://panax.io/shell" and local-name()="shell"]/*');
            for (let dr in nodes) {
                var node = nodes[dr];
                xdom.data.remove(shell.selectSingleNode('//*[namespace-uri()="' + node.namespaceURI + '" and local-name()="' + node.localName + '"]'));
                shell.documentElement.appendChild(node);
            }

            var nodes = document.selectNodes('//*[namespace-uri()="http://panax.io/sitemap" and local-name()="sitemap"]');
            for (let dr in nodes) {
                var node = nodes[dr];
                xdom.data.remove(shell.selectSingleNode('//*[namespace-uri()="' + node.namespaceURI + '" and local-name()="' + node.localName + '"]'));
                shell.documentElement.appendChild(node);
            }
            xdom.dom.shell = shell;
        }
    }
    else {
        xdom.stores[xdom.data.hashTagName(document)] = document;
    }
}

xdom.data.loadDependencies = async function (xml_document, on_complete) {
    var library = xdom.data.getTransformations(xml_document);
    await xdom.library.load(library, on_complete);
}

xdom.data.applyTransforms = function (original_context, transforms) {
    var context = original_context;
    var context_transforms;
    if (context instanceof xdom.Store) {
        context_transforms = context.library;
    } else {
        context_transforms = xdom.library;
    }
    if (context) {
        ////context = context.selectSingleNode("ancestor-or-self::*[@x:transforms]");
        //context = (context.nodeName == "#document" ? context.documentElement : context);
        var target_id = (context.documentElement || context).getAttribute("x:id");
        if (!transforms) {
            transforms = xdom.data.getTransformationFileName(context).split(";");
            //transforms.pop();
        }

        if (transforms.length) {
            for (let t = 0; t < transforms.length; t++) {
                transforms[t] = (transforms[t] || '').replace(/;\s*$/, '');
                if (!transforms[t]) continue;
                if (!context_transforms[transforms[t]]) {
                    ////console.error("xdom.data.applyTransforms: File " + transforms[t] + " is not ready.");
                    //fetchFiles(transforms[t]);
                    if (!(xdom.browser.isIOS())) {
                        var xsl = xdom.xml.createDocument(`                                                                  
                    <xsl:stylesheet version="1.0"                                                                            
                        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">                                                    
                        <xsl:import href="${transforms[t]}" />                                                             
                    </xsl:stylesheet>`);
                        context = xdom.xml.transform(context, xsl);
                    } else {
                        context = xdom.xml.transform(context, transforms[t]);
                    }
                } else {
                    context = xdom.xml.transform(context, context_transforms[transforms[t]]);//, context);
                }
            }
            if (target_id) {
                context = (context.selectSingleNode('//*[@x:id="' + target_id + '"]') || context);
                var target_node = original_context.selectSingleNode('//*[@x:id="' + target_id + '"]')
                if (target_node && target_node.parentNode && context !== target_node && context.selectSingleNode('//*[@x:id="' + target_id + '"]')) {
                    target_node.parentNode.replaceChild(context, target_node);
                    //context.reseed();
                }
            }
        }
    }
    return context;//(xdom.data.find(context) || context);
}

xdom.data.removeSelections = function (nodes) {
    /*USE FOR DEMOSTRATION*/
    //    var xsl_transform = xdom.xml.createDocument('\
    //<xsl:stylesheet version="1.0" \
    // xmlns:xsl="http://www.w3.org/1999/XSL/Transform"\
    // xmlns:x="http://panax.io/xdom">\
    //  <xsl:output method="xml" indent="no"/>\
    //  <xsl:template match="@* | node() | text()">\
    //    <xsl:copy>\
    //      <xsl:apply-templates select="@*"/>\
    //      <xsl:apply-templates select="node() | text()"/>\
    //    </xsl:copy>\
    //  </xsl:template>\
    //  <xsl:template match="@x:selected"/>\
    //</xsl:stylesheet>');
    //    xdom.updateData(xdom.xml.transform(xdom.stores.active, xsl_transform));

    for (let node = 0; node < nodes.length; ++node) {
        nodes[node].removeAttribute('x:selected');
        //    //xdom.data.removeSelections(nodes[node].selectNodes('../*[@x:selected]'))
    }
}


xdom.data.updateNode = async function (target, attribute, value, refreshDOM, action, srcElement, caller) {
    srcElement = (srcElement || event && event.srcElement);
    xdom.dom.saveState(srcElement);
    target = xdom.data.deepFind(target);
    if (!target) {
        console.error('Target may have changed');
    }
    var trigger_node = (srcElement || {}).id && xdom.data.deepFind(srcElement.id);
    value = xdom.data.coalesce(value, (attribute == '@value' ? xdom.data.getValue(srcElement) : undefined));
    if ((attribute || '').constructor == [].constructor) {
        attributes = attribute;
    } else {
        attributes = [];
        attributes.push(new Object().push(attribute, value));
    }
    var changed = undefined;
    for (let item = 0; item < attributes.length; ++item) {
        var attribute = Object.keys(attributes[item]).join("");
        var value = attributes[item][attribute];
        if (xdom.debug["xdom.data.update"]) {
            console.log("uid: " + uid)
            console.log("\tAttribute: " + attribute)
            console.log("\tValue: " + value)
        }
        if (attribute == '@value' || attribute == '@x:deleting' || attribute == '@x:checked') {
            if (caller && (caller.name || "").match(/onchange|onblur|onpropertychange/)) {
                xdom.data.history.undo.push(xdom.xml.createDocument(xdom.stores.active));
                xdom.data.history.redo = [];
            }
        }
        var regex = new RegExp(xdom.string.trim(attribute) + '\\b', 'g');
        if (typeof (((target || {}).ownerDocument || {}).setProperty) != "undefined") {
            var current_namespaces = xdom.xml.getNamespaces(target.ownerDocument.getProperty("SelectionNamespaces"));
            if (!current_namespaces["x"]) {
                current_namespaces["x"] = "http://panax.io/xdom";
                target.ownerDocument.setProperty("SelectionNamespaces", xdom.json.join(current_namespaces, { "separator": " " }));
            }
        }
        //if (target.selectSingleNode('self::x:*') || target.getAttribute("x:readonly") == 'true()' || target.getAttribute("x:readonly") != 'false()' && ((target.getAttribute("x:readonly") || '').match(regex))) {
        //    //if (xdom.debug) {
        //        console.warn('Attribute ' + attribute + ' in ' + target.nodeName + ' can\'t be modified.')
        //    //}
        //    return false;
        //}
        var prefix = attribute.match(/^(@?)([^:]+)/).pop();
        /*Se quita esta sección porque ya se hace la validación del namespace en el setAttribute*//*
        if (xdom.xml.namespaces[prefix] && !xdom.xml.lookupNamespaceURI(target.ownerDocument.documentElement, xdom.xml.namespaces[prefix])) {
            target.ownerDocument.documentElement.setAttribute("xmlns:" + prefix, xdom.xml.namespaces[prefix]);
            if (typeof (target.ownerDocument.setProperty) != "undefined") {
                var xml_namespaces = xdom.xml.transform(target.ownerDocument, xdom.library["xdom/normalize_namespaces.xslt"]);
                target.ownerDocument.setProperty("SelectionNamespaces", xdom.xml.createNamespaceDeclaration(xml_namespaces));
            }
        }*/

        if (attribute == 'data()') {
            if (value) {
                if (action == 'append') {
                    target.appendChild(value.documentElement);
                } else {
                    target.innerHTML = value.documentElement.outerHTML;
                }
            }
            refreshDOM = true;
        } else if (attribute == 'text()') {
            if (String(value).match(/^=/g).length > 0) {
                target.setAttribute('formula', value, false);
            }
            target.innerHTML = value;
            refreshDOM = true;
        } else if (attribute.match("^@")) {
            if (value && target.getAttribute('x:format') == 'money') {
                value = value.replace(/[^\d\.-]/gi, '');
            }
            var attribute_ref = (target.selectSingleNode(attribute) || {});
            if (attribute_ref.value == null || attribute_ref.value != (value || String(value)) || value && !String(value).match("{{") && attribute.match("^@(source):") && (!(target.selectSingleNode(attribute.replace(/^@/, ""))))) {
                if (xdom.xml.namespaces[prefix] == "http://panax.io/xdom") {
                    var attribute_base_name = attribute.match(/^(@?)([^:]+):?(.*)/).pop();
                    if (!target.selectNodes("@initial:" + attribute_base_name).length) {
                        target.setAttribute("initial:" + attribute_base_name, (target.getAttribute("initial:" + attribute_base_name) || attribute_ref.value || (attribute_base_name == 'checked' ? 'false' : '')), false);
                    }
                    //if (!target.selectNodes("@prev:" + attribute_base_name).length) {
                    //    target.setAttribute("prev:" + attribute_base_name, (attribute_ref.value || (attribute_base_name == 'checked' ? 'false' : '')));
                    //}
                }
                target.setAttribute(attribute.substring(1), (value || String(value)), false);//(value || "") //(value || "null"));
                changed = true;

                if (attribute.match("^@(source):")) {
                    //TODO: Revisar si este código aún tiene efecto o si se cubre con el binding
                    //if ((value || '').match("{{")) {
                    //    value = "";
                    //} else {
                    //    var request = value.replace(/^\w+:/, '');
                    //    value = xdom.data.binding.handleRequest({
                    //        "request": request
                    //        , "target_attribute": attribute.replace(/^@/, "")
                    //        , "xhr": {
                    //            "async": true
                    //            , "target": target
                    //            , "type": (attribute.match("^@source:") ? "table" : "scalar")
                    //        }
                    //    });
                    //}
                    //attributes.push(new Object().push(attribute.replace(/^@/, ""), value));
                } else if (attribute == ("@value") && attributes.find(element => element["@text"]) === undefined) {
                    var catalog = target.selectSingleNode(attribute.replace(/^@\w+:/, 'source:'));
                    if (catalog) {
                        attributes.push(new Object().push("@text", (catalog.selectSingleNode('*[normalize-space(' + attribute + ')="' + String(value).replace(/"/, '\\"') + '"]/@text') || {}).value || ""));
                    } else if (trigger_node && trigger_node.selectSingleNode('self::x:r')) {
                        attributes.push(new Object().push("@text", (trigger_node.getAttribute((trigger_node.getAttribute("x:text_field") || '').substring(1) || 'text') || (value || "") || "")));
                    }
                } else if (attribute == ("@text") && attributes.find(element => element["@value"]) === undefined) {
                    var catalog = target.selectSingleNode(attribute.replace(/^@\w+:/, 'source:'));
                    if (catalog) {
                        attributes.push(new Object().push("@value", (catalog.selectSingleNode('*[normalize-space(' + attribute + ')="' + String(value).replace(/"/, '\\"') + '"]/@value') || {}).value || ""));
                    } else if (trigger_node && trigger_node.selectSingleNode('self::x:r')) {
                        attributes.push(new Object().push("@value", (trigger_node.getAttribute((trigger_node.getAttribute("x:value_field") || '').substring(1) || 'value') || (value || "") || "")));
                    }
                }
                refreshDOM = refreshDOM !== undefined ? refreshDOM : undefined;
            }
        } else {
            if (action == 'append') {
                xdom.dom.insertAfter(value);
            } else {
                //if (target.selectSingleNode('@' + attribute)) {
                //    var node_exists = xdom.data.remove(target.selectSingleNode(attribute));
                //}
                if (!value && (typeof (value) != "boolean")) {
                    var node_exists = xdom.data.remove(target.selectSingleNode(attribute));
                }
                if (value != undefined && value !== '' && typeof (value.selectSingleNode) == 'undefined') {
                    var node = target.selectSingleNode(attribute);
                    if (!node) {
                        var namespaces = xdom.xml.createNamespaceDeclaration(xdom.stores.active)
                        node = xdom.xml.createDocument("<" + attribute + " " + namespaces + ">" + String(value) + "</" + attribute + ">");
                    }
                    value = node;
                }
                if (value && value.documentElement) {
                    //var attrs = value.documentElement.selectNodes('@*');
                    //for (let attr in attrs) {
                    //    attrs[attr] 
                    //}
                    //xdom.data.remove(target.selectSingleNode(value.selectSingleNode("//*[1]").nodeName));
                    var for_value = value.documentElement.getAttribute("command");
                    var node_name = value.documentElement.nodeName;
                    var node = target.selectSingleNode(node_name + '[@command="' + for_value + '"]')
                    if (for_value && node) {
                        node.parentNode.replaceChild(value.documentElement, node);
                    } else {
                        xdom.dom.appendChild(target, value);
                    }
                    var source_transform = target.selectSingleNode("ancestor-or-self::*[@transforms:sources]/@transforms:sources");
                    if (source_transform && source_transform.value) {
                        await xdom.data.applyTransforms(target, [source_transform.value]);
                        target = xdom.stores.active.selectSingleNode('//*[@x:id="' + uid + '"]');
                    }
                }
            }
            changed = true;
            if (attribute.match("^source:")) {
                var target_attribute = attribute.replace(/^source:/, "@x:");
                var source_record = null;
                var target_value = (src.getAttribute("value") || src.getAttribute("initial:value"))
                if (String(parseFloat(target_value)) != 'NaN') {
                    source_record = src.selectSingleNode(attribute + '/*[number(' + target_attribute + ')="' + parseFloat(target_value) + '"]/' + target_attribute)
                } else {
                    source_record = (src.selectSingleNode(attribute + '/*[' + target_attribute + '="' + target_value + '"]/' + target_attribute) || new xdom.data.EmptyXML());
                }
                if (source_record) {
                    attributes.push(new Object().push(target_attribute, (source_record.value || "")));
                    attributes.push(new Object().push("@text", (source_record.selectSingleNode("../@text").value || "")));
                }
            }
            refreshDOM = refreshDOM !== undefined ? refreshDOM : undefined;
        }
    }
    return !!changed;
}

xdom.data.asyncUpdate = async function (target, attribute, value, refreshDOM, action, cascade, event, caller) {
    xdom.dom.saveState();
    var srcElement = ((event || {}).srcElement || (caller && caller.hasOwnProperty('arguments') ? caller.arguments[0] : {}).srcElement);
    srcElement = ((event || {}).srcElement);
    var xdom_global_refresh_disabled = srcElement && srcElement.ownerDocument ? srcElement.ownerDocument.xdom_global_refresh_disabled : false;
    var xdom_global_refresh_enabled = (typeof (xdom_global_refresh_disabled) != "undefined" ? !xdom_global_refresh_disabled : undefined);
    if (xdom_global_refresh_enabled) xdom_global_refresh_enabled = undefined;
    var trigger = (event && caller != xdom.data.cascade ? event.srcElement : undefined);
    var settings = {}
    if (refreshDOM !== undefined) {
        settings["refresh"] = refreshDOM;
    }

    if (arguments.length == 0) {
        refreshDOM = true;
    } else if (arguments[0] && arguments[0].constructor === {}.constructor) {
        settings = xdom.json.merge(settings, arguments[0]);
        target = settings["target"];
        if (!settings.hasOwnProperty("target") && !settings.hasOwnProperty("attributes") && !settings.hasOwnProperty("uid") && !settings.hasOwnProperty("refresh")) {
            var attributes = settings;
            settings = {};
            settings["attributes"] = [attributes];
        }
    } else if (arguments.length == 1 && typeof (arguments[0]) == 'boolean') {
        refreshDOM = arguments[0];
        target = undefined;
    }

    if (value && typeof (value) == 'object' && !value.documentElement) { return false; }

    var uid = xdom.data.coalesce(settings["uid"], typeof (target) == "string" ? target : undefined);
    var attribute = xdom.data.coalesce(settings["attribute"], attribute, '@value');
    var value = settings.hasOwnProperty("value") ? settings["value"] : xdom.data.coalesce(value, (attribute == '@value' ? xdom.data.getValue(trigger) : undefined));

    var action = xdom.data.coalesce(settings["action"], action, 'replace'); //[replace | append]
    var cascade = xdom.data.coalesce(settings["cascade"], cascade, true);
    var src;
    trigger = trigger && trigger.id ? xdom.stores.active.selectSingleNode("//*[@x:id='" + trigger.id + "']") : undefined;

    if (target && typeof (target.selectSingleNode) != 'undefined') {
        src = target;
        uid = target.getAttribute("x:id")
    } else if (uid) {
        src = xdom.stores.active.selectSingleNode("//*[@x:id='" + uid + "']");
    } else if (event && event.srcElement) {
        uid = (xdom.dom.findClosestElementWithId(event.srcElement) || event.srcElement).id;
        src = xdom.stores.active.selectSingleNode("//*[@x:id='" + uid + "']/ancestor-or-self::*[not(contains(namespace-uri(),'http://panax.io/xdom'))][1]");
    }
    if (!src) {
        console.warn("Reference to a record is needed");
        return;
    }
    //if (uid) {
    //    xdom.dom.activeElementId = uid
    //    xdom.stores.active.selectSingleNode("/*").setAttribute("x:focus", uid)
    //}
    if (!uid) {
        var uid = Math.random().toString();
        if (!src.getAttribute("xmlns:x")) {
            src.setAttribute("xmlns:x", xdom.xml.namespaces["x"], false);
        }
        src.setAttribute("x:id", uid, false);
    }
    var start_date = new Date();
    if (!src) {
        console.error("Couldn't update data, the source might have changed. Trying to reload.");
        xdom.dom.refresh();
        return;
    }
    if (xdom.stores.active.selectSingleNode('//*[@x:id="' + src.getAttribute("x:id") + '"]') && xdom.stores.active.selectSingleNode('//*[@x:id="' + src.getAttribute("x:id") + '"]') !== src) {
        src = xdom.stores.active.selectSingleNode('//*[@x:id="' + src.getAttribute("x:id") + '"]');
    }
    var $bindings = xdom.data.binding.sources;
    var dependants = {};
    var attributes;
    if (settings["attributes"]) {
        attributes = settings["attributes"];
    } else {
        attributes = [];
        attributes.push(new Object().push(attribute, value));
    }
    var changed = undefined;
    for (let item = 0; item < attributes.length; ++item) {
        attribute = Object.keys(attributes[item]).join("");
        value = attributes[item][attribute];
        if (xdom.debug["xdom.data.update"]) {
            console.log("uid: " + uid)
            console.log("\tAttribute: " + attribute)
            console.log("\tValue: " + value)
        }
        if (attribute == '@value' || attribute == '@x:deleting' || attribute == '@x:checked') {
            if (caller && (caller.name || "").match(/onchange|onblur|onpropertychange/)) {
                xdom.data.history.undo.push(xdom.xml.createDocument(xdom.stores.active));
                xdom.data.history.redo = [];
            }
        }
        var regex = new RegExp(xdom.string.trim(attribute) + '\\b', 'g');
        if (typeof (src.ownerDocument.setProperty) != "undefined") {
            var current_namespaces = xdom.xml.getNamespaces(src.ownerDocument.getProperty("SelectionNamespaces"));
            if (!current_namespaces["x"]) {
                current_namespaces["x"] = "http://panax.io/xdom";
                src.ownerDocument.setProperty("SelectionNamespaces", xdom.json.join(current_namespaces, { "separator": " " }));
            }
        }
        //if (src.selectSingleNode('self::x:*') || src.getAttribute("x:readonly") == 'true()' || src.getAttribute("x:readonly") != 'false()' && ((src.getAttribute("x:readonly") || '').match(regex))) {
        //    //if (xdom.debug) {
        //        console.warn('Attribute ' + attribute + ' in ' + src.nodeName + ' can\'t be modified.')
        //    //}
        //    return false;
        //}
        var prefix = attribute.match(/^(@?)([^:]+)/).pop();
        if (xdom.xml.namespaces[prefix] && !xdom.xml.lookupNamespaceURI(src.ownerDocument.documentElement, xdom.xml.namespaces[prefix])) {
            src.ownerDocument.documentElement.setAttribute("xmlns:" + prefix, xdom.xml.namespaces[prefix], false);
            if (typeof (src.ownerDocument.setProperty) != "undefined") {
                var xml_namespaces = xdom.xml.transform(src.ownerDocument, xdom.library["xdom/normalize_namespaces.xslt"]);
                src.ownerDocument.setProperty("SelectionNamespaces", xdom.xml.createNamespaceDeclaration(xml_namespaces));
            }
        }

        if (attribute == 'data()') {
            if (value) {
                if (action == 'append') {
                    src.appendChild(value.documentElement);
                } else {
                    src.innerHTML = value.documentElement.outerHTML;
                }
            }
            refreshDOM = true;
        } else if (attribute == 'text()') {
            if (String(value).match(/^=/g).length > 0) {
                src.setAttribute('formula', value, false);
            }
            src.innerHTML = value;
            refreshDOM = true;
        } else if (attribute.match("^@")) {
            if (value && src.getAttribute('x:format') == 'money') {
                value = value.replace(/[^\d\.-]/gi, '');
            }
            var attribute_ref = (src.selectSingleNode(attribute) || {});
            if (attribute_ref.value == null || attribute_ref.value != (value || String(value)) || value && !String(value).match("{{") && attribute.match("^@(source):") && (!(src.selectSingleNode(attribute.replace(/^@/, ""))) || !$bindings[src.getAttribute("x:id")].formulas[value].updated)) {
                dependants = xdom.json.merge(dependants, ($bindings[src.getAttribute("x:id")] || { "dependants": {} }).dependants[attribute]);
                if (xdom.xml.namespaces[prefix] == "http://panax.io/xdom") {
                    var attribute_base_name = attribute.match(/^(@?)([^:]+):?(.*)/).pop();
                    if (!src.selectNodes("@initial:" + attribute_base_name).length) {
                        src.setAttribute("initial:" + attribute_base_name, (src.getAttribute("initial:" + attribute_base_name) || attribute_ref.value || (attribute_base_name == 'checked' ? 'false' : '')), false);
                    }
                    if (!src.selectNodes("@prev:" + attribute_base_name).length) {
                        src.setAttribute("prev:" + attribute_base_name, (attribute_ref.value || (attribute_base_name == 'checked' ? 'false' : '')), false);
                    }
                }
                src.setAttribute(attribute.substring(1), (value || String(value)), false);//(value || "") //(value || "null"));
                changed = true;

                if (attribute.match("^@(source):")) {
                    //TODO: Revisar si este código aún tiene efecto o si se cubre con el binding
                    //if ((value || '').match("{{")) {
                    //    value = "";
                    //} else {
                    //    var request = value.replace(/^\w+:/, '');
                    //    value = xdom.data.binding.handleRequest({
                    //        "request": request
                    //        , "target_attribute": attribute.replace(/^@/, "")
                    //        , "xhr": {
                    //            "async": true
                    //            , "target": src
                    //            , "type": (attribute.match("^@source:") ? "table" : "scalar")
                    //        }
                    //    });
                    //}
                    //attributes.push(new Object().push(attribute.replace(/^@/, ""), value));
                } else if (attribute == ("@value") && attributes.find(element => element["@text"]) === undefined) {
                    var catalog = src.selectSingleNode(attribute.replace(/^@\w+:/, 'source:'));
                    if (catalog) {
                        attributes.push(new Object().push("@text", (catalog.selectSingleNode('*[normalize-space(' + attribute + ')="' + String(value).replace(/"/, '\\"') + '"]/@text') || {}).value || ""));
                    } else if (trigger && trigger.selectSingleNode('self::x:r')) {
                        attributes.push(new Object().push("@text", (trigger.getAttribute((trigger.getAttribute("x:text_field") || '').substring(1) || 'text') || (value || "") || "")));
                    }
                } else if (attribute == ("@text") && attributes.find(element => element["@value"]) === undefined) {
                    var catalog = src.selectSingleNode(attribute.replace(/^@\w+:/, 'source:'));
                    if (catalog) {
                        attributes.push(new Object().push("@value", (catalog.selectSingleNode('*[normalize-space(' + attribute + ')="' + String(value).replace(/"/, '\\"') + '"]/@value') || {}).value || ""));
                    } else if (trigger && trigger.selectSingleNode('self::x:r')) {
                        attributes.push(new Object().push("@value", (trigger.getAttribute((trigger.getAttribute("x:value_field") || '').substring(1) || 'value') || (value || "") || "")));
                    }
                }
                refreshDOM = refreshDOM !== undefined ? refreshDOM : undefined;
            }
        } else {
            dependants = xdom.json.merge(dependants, ($bindings[src.getAttribute("x:id")] || { "dependants": {} }).dependants[attribute]);
            if (action == 'append') {
                xdom.dom.insertAfter(value);
            } else {
                //if (src.selectSingleNode('@' + attribute)) {
                //    var node_exists = xdom.data.remove(src.selectSingleNode(attribute));
                //}
                if (!value && (typeof (value) != "boolean")) {
                    var node_exists = xdom.data.remove(src.selectSingleNode(attribute));
                }
                if (value != undefined && value !== '' && typeof (value.selectSingleNode) == 'undefined') {
                    var node = src.selectSingleNode(attribute);
                    if (!node) {
                        var namespaces = xdom.xml.createNamespaceDeclaration(xdom.stores.active)
                        node = xdom.xml.createDocument("<" + attribute + " " + namespaces + ">" + String(value) + "</" + attribute + ">");
                    }
                    value = node;
                }
                if (value && value.documentElement) {
                    //var attrs = value.documentElement.selectNodes('@*');
                    //for (let attr in attrs) {
                    //    attrs[attr] 
                    //}
                    //xdom.data.remove(src.selectSingleNode(value.selectSingleNode("//*[1]").nodeName));
                    var for_value = value.documentElement.getAttribute("command");
                    var node_name = value.documentElement.nodeName;
                    var node = src.selectSingleNode(node_name + '[@command="' + for_value + '"]')
                    if (for_value && node) {
                        node.parentNode.replaceChild(value.documentElement, node);
                    } else {
                        xdom.dom.appendChild(src, value);
                    }
                    var source_transform = src.selectSingleNode("ancestor-or-self::*[@transforms:sources]/@transforms:sources");
                    if (source_transform && source_transform.value) {
                        await xdom.data.applyTransforms(src, [source_transform.value]);
                        src = xdom.stores.active.selectSingleNode('//*[@x:id="' + uid + '"]');
                    }
                }
            }
            changed = true;
            if (attribute.match("^source:")) {
                var target_attribute = attribute.replace(/^source:/, "@x:");
                var source_record = null;
                var target_value = (src.getAttribute("value") || src.getAttribute("prev:value"))
                if (String(parseFloat(target_value)) != 'NaN') {
                    source_record = src.selectSingleNode(attribute + '/*[number(' + target_attribute + ')="' + parseFloat(target_value) + '"]/' + target_attribute)
                } else {
                    source_record = (src.selectSingleNode(attribute + '/*[' + target_attribute + '="' + target_value + '"]/' + target_attribute) || new xdom.data.EmptyXML());
                }
                if (source_record) {
                    attributes.push(new Object().push(target_attribute, (source_record.value || "")));
                    attributes.push(new Object().push("@text", (source_record.selectSingleNode("../@text").value || "")));
                }
            }
            refreshDOM = refreshDOM !== undefined ? refreshDOM : undefined;
        }
    }
    if (xdom.data.coalesce(xdom_global_refresh_enabled, refreshDOM, changed)) {
        //xdom.data.history.redo = [];
        src = xdom.data.deepFind(src);
        //if (!xdom.stores.active.contains(src)) {
        //    xdom.stores.active = xdom.xml.createDocument(xdom.stores.active);
        //    src = xdom.stores.active.selectSingleNode('//*[@x:id="' + uid + '"]');
        //}
        if (!xdom.stores.active.find(src)) {
            var target = xdom.deprecated.getMasterDocument(src);
            await target.render();
        } else {
            var context = src.selectSingleNode("ancestor-or-self::*[@x:transforms]");
            xdom.data.applyTransforms(context);

            if (srcElement && srcElement.type == 'date' && caller.name.match(/onchange/)) {
                xdom.listener.keypress.tabKey = true;
            }
            xdom.stores.active.reseed();
        }

        //if (xdom.data.update.caller && xdom.data.update.caller.name.match(/onchange|onblur|onpropertychange/)) {
        if (event && event.srcElement && (event.srcElement.name || "").match(/onchange|onblur|onpropertychange/)) {
            if (this.delayRefresh != undefined) {
                window.clearTimeout(this.delayRefresh); this.delayRefresh = undefined;
            }
            this.delayRefresh = setTimeout(function () {
                xdom.dom.refresh();
            }.apply(this, arguments), 5);
        } else {
            xdom.dom.refresh();
        }

    }
    if (xdom.session.live.running) xdom.session.saveSession();
    //xdom.session.setData(xdom.stores.active);
}

xdom.data.submit = async function (settings) { }
xdom.data.submit = async function (data, xhr_settings) {
    if (!await xdom.session.checkStatus()) {
        return;
    }
    if (arguments.length == 1 && arguments[0].constructor === {}.constructor) {
        settings = arguments[0];
    } else if (arguments.length == 1 && typeof arguments[0] == 'string') {
        var uid = arguments[0];
        xdom.stores.active = xdom.xml.transform(xdom.stores.active, xdom.library["xdom/normalize_namespaces.xslt"]);
        data = xdom.stores.active.selectSingleNode('//*[@x:id="' + uid + '"]');
    }
    var data = (data || xdom.stores.active)
    data = (data.documentElement || data);
    this.prepareData = function (data) {
        if (settings["prepareData"] && settings["prepareData"].apply) {
            settings["prepareData"].apply(this, [data])
        };
        return data;
    }

    var settings = (settings || {});
    var xhr_settings = xdom.json.merge({ headers: { "Accept": 'text/xml', "Content-Type": 'text/xml' } }, (xhr_settings || settings["xhr"]));
    var onsuccess = (xhr_settings["onSuccess"] || function () { });
    if (data) {
        this.prepareData(data);

        data.setAttributes({
            "session:user_id": (data.getAttribute("session:user_id") || xdom.session.getUserId())
            , "session:user_login": (data.getAttribute("session:user_login") || xdom.session.getUserLogin())
            , "session:status": (xdom.session.getUserLogin() ? "authorized" : "unauthorized")
        });

        var target = data.selectSingleNode('//*[@x:reference]')
        var datarows = data.selectNodes('px:data/px:dataRow');

        if (target && data.selectSingleNode('px:data/px:dataRow[not(@identity)]')) {
            for (let xDocument in xdom.stores) {
                source = xdom.stores[xDocument].selectSingleNode('//*[@x:id="' + target.getAttribute('x:reference') + '"]');
                if (!source || (window.top || window).location.hash == xDocument) { continue; }
                for (let dr in datarows) {
                    var datarow = datarows[dr];
                    var target = xdom.data.find(datarow, source);
                    if (target) {
                        xdom.data.replace(target, datarow);
                    } else if (source.selectSingleNode('self::px:dataRow')) {
                        xdom.dom.insertAfter(datarow, source);
                    } else if (!source.selectSingleNode('px:data')) {
                        source.appendChild(datarow.parentNode);
                    }
                    else {
                        source.selectSingleNode('px:data').appendChild(datarow);
                    }
                }
                xdom.stores.active = source.ownerDocument;
                xdom.dom.refresh();
                //window.history.back();
                return
            }
            if (confirm("¿Desea guardar el registro en este momento?")) {
                xdom.data.remove(target.selectSingleNode('@x:reference'));
            } else {
                return;
            }
            //xdom.dom.navigateTo("#proveedor:edit");
        }

        data = xdom.xml.createDocument(data).documentElement;
        var submit_data;
        if (data.getAttribute("transforms:submit")) {
            submit_data = xdom.xml.transform(data.ownerDocument, data.getAttribute("transforms:submit"));
        }
        var payload = xdom.xml.createDocument('<x:post xmlns:x="http://panax.io/xdom"><x:source>' + xdom.xml.toString(data) + '</x:source>' + (submit_data ? '<x:submit>' + xdom.xml.toString(submit_data) + '</x:submit>' : '') + '</x:post>');
        var xhr = new xdom.xhr.Request(xdom.manifest.server["endpoints"]["post"], xdom.json.merge(xhr_settings, {
            onSuccess: function (Response, Request) {
                var results = Response.value;
                if (onsuccess && onsuccess.apply) {
                    onsuccess.apply(this, arguments);
                }
                if (Response.type == 'json' || Response.type == 'script') {
                    xdom.data.update(data.getAttribute("x:id"), "@x:trid", results.recordSet[0][""]);
                }
                else if (Response.type == "xml") {
                    if (Response.responseXML.documentElement && Response.responseXML.documentElement.selectSingleNode('/x:message')) {
                        if (!(this.subscribers)) {
                            (xdom.dom.findClosestDataNode(Request.srcElement) || xdom.stores.active.documentElement).appendChild(Response.responseXML.documentElement);
                            xdom.dom.refresh({ after: function () { return null; } });
                        } else {
                            var target = xdom.stores.active;
                            if (target) {
                                target.documentElement.appendChild(Response.responseXML.documentElement);
                            } else {
                                console.warn(Response.responseXML)
                            }
                        }
                    }
                }

                if (results && results.message) {
                    alert(results.message);
                }
                //xdom.data.update(data.getAttribute("x:id"), "@state:submitted", "true", false);
                for (let datarow of datarows) {
                    xdom.data.update(datarow, "@state:submitted", "true", false);
                }
            }
            , onException: function (Response, Request) {
                if (Response.status == 304) {
                    alert("No hay cambios");
                }
                else if (Response.type == "xml") {
                    if (Response.document.documentElement && Response.document.documentElement.selectSingleNode('/x:message')) {
                        if (!(this.subscribers)) {
                            (xdom.dom.findClosestDataNode(Request.srcElement) || xdom.stores.active.documentElement).appendChild(Response.responseXML.documentElement);
                            xdom.dom.refresh({ after: function () { return null; } });
                        } else {
                            var target = xdom.stores.active;
                            if (target) {
                                target.documentElement.appendChild(Response.document.documentElement);
                            } else {
                                console.warn(Response.document)
                            }
                        }
                    }
                } else if (Response.status != 401) {
                    alert("No se pudo guardar la información, intente de nuevo");
                }
            }
            , onComplete: function (Response, Request) {
                //xdom.data.update(data.getAttribute("x:id"), "@state:submitting", "false");
                for (let datarow of datarows) {
                    xdom.data.update(datarow, "@state:submitting", "false");
                }
            }
        }));
        //var nodes = payload.selectNodes('//source:value|//@source:value');
        //xdom.data.remove(nodes);
        //xdom.data.update(data.getAttribute("x:id"), "@state:submitting", "true");
        for (let datarow of datarows) {
            xdom.data.update(datarow, "@state:submitting", "true");
        }
        xhr.send(payload);


        //xhr.upload.onprogress = p => {
        //    console.log(Math.round((p.loaded / p.total) * 100) + '%');
        //}
    }
}
/*<------------------------------- deprecate*/
xdom.callStack = [];

Object.defineProperty(xdom.callStack, 'add', {
    value: function (ref) {
        this.push(ref);
    },
    writable: true, enumerable: false, configurable: false
});

Object.defineProperty(xdom.callStack, 'remove', {
    value: function (ref) {
        this.reverse().map((obj, index, array) => {
            if (obj.src === ref.src && obj.method === ref.method) {
                array.splice(index, 1);
                array.reverse();
                return;
            }
        });
    },
    writable: true, enumerable: false, configurable: false
});

xdom.xml.compare = function (document1, document2, stop_at_first_change) {
    let xsl_compare = xdom.xml.transform(document1, xdom.xml.createDocument(`<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:c="http://panax.io/xml/compare" version="1.0" id="panax_xml_compare_xsl"><xsl:output method="xml"></xsl:output><xsl:strip-space elements="*"></xsl:strip-space><xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'"></xsl:variable><xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"></xsl:variable><xsl:template match="/"><xsl:element name="xsl:stylesheet"><xsl:copy-of select="//namespace::*"/><xsl:attribute name="version">1.0</xsl:attribute><xsl:element name="xsl:template"><xsl:attribute name="match">/</xsl:attribute><xsl:element name="results"><xsl:element name="xsl:apply-templates"></xsl:element></xsl:element></xsl:element><xsl:element name="xsl:template"><xsl:attribute name="match">*</xsl:attribute><xsl:element name="change" namespace="http://panax.io/xml/compare"><xsl:attribute name="c:position"><xsl:value-of select="'{count(preceding-sibling::*)+1}'"></xsl:value-of></xsl:attribute><xsl:attribute name="c:namespace"><xsl:value-of select="'{namespace-uri()}'"></xsl:value-of></xsl:attribute><xsl:attribute name="c:name"><xsl:value-of select="'{name()}'"></xsl:value-of></xsl:attribute><xsl:attribute name="c:type"><xsl:text>Node</xsl:text></xsl:attribute><xsl:element name="xsl:copy-of"><xsl:attribute name="select">@*</xsl:attribute></xsl:element><xsl:element name="xsl:apply-templates"></xsl:element></xsl:element></xsl:element><xsl:element name="xsl:template"><xsl:attribute name="match">text()</xsl:attribute><xsl:element name="change" namespace="http://panax.io/xml/compare"><xsl:attribute name="c:type"><xsl:text>Text</xsl:text></xsl:attribute><xsl:attribute name="c:position"><xsl:value-of select="'{count(preceding-sibling::*)}'"></xsl:value-of></xsl:attribute><xsl:attribute name="c:text"><xsl:value-of select="'{.}'"></xsl:value-of></xsl:attribute></xsl:element></xsl:element><xsl:apply-templates></xsl:apply-templates></xsl:element></xsl:template><xsl:template name="escape-xml"><xsl:param name="wrapper">&quot;</xsl:param><xsl:param name="text"></xsl:param><xsl:if test="$text != ''"><xsl:variable name="head" select="substring($text, 1, 1)"></xsl:variable><xsl:variable name="tail" select="substring($text, 2)"></xsl:variable><xsl:choose><xsl:when test="$head = '&amp;'">&amp;amp;</xsl:when><xsl:when test="$head = '&lt;'">&amp;lt;</xsl:when><xsl:when test="$head = '&gt;'">&amp;gt;</xsl:when><xsl:when test="$head = '&quot;'">&amp;quot;</xsl:when><xsl:when test="$wrapper=&quot;'&quot; and $head = &quot;'&quot;">&amp;apos;</xsl:when><xsl:otherwise><xsl:value-of select="$head"></xsl:value-of></xsl:otherwise></xsl:choose><xsl:call-template name="escape-xml"><xsl:with-param name="text" select="$tail"></xsl:with-param></xsl:call-template></xsl:if></xsl:template><xsl:template name="escape-quot"><xsl:param name="string"></xsl:param><xsl:variable name="quot">&quot;</xsl:variable><xsl:variable name="escaped-quot">&amp;quot;</xsl:variable><xsl:text>&quot;</xsl:text><xsl:choose><xsl:when test="contains($string, $quot)"><xsl:value-of select="substring-before($string, $quot)"></xsl:value-of><xsl:text>&quot;,'&quot;',</xsl:text><xsl:call-template name="escape-quot"><xsl:with-param name="string" select="substring-after($string, $quot)"></xsl:with-param></xsl:call-template><xsl:text>,&quot;</xsl:text></xsl:when><xsl:otherwise><xsl:value-of select="$string"></xsl:value-of></xsl:otherwise></xsl:choose><xsl:text>&quot;</xsl:text></xsl:template><xsl:template name="escape-apos"><xsl:param name="string"></xsl:param><xsl:choose><xsl:when test="contains($string, &quot;'&quot;)"><xsl:value-of select="substring-before($string, &quot;'&quot;)"></xsl:value-of><xsl:text>'</xsl:text><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="substring-after($string, &quot;'&quot;)"></xsl:with-param></xsl:call-template></xsl:when><xsl:otherwise><xsl:value-of select="$string"></xsl:value-of></xsl:otherwise></xsl:choose></xsl:template><xsl:template match="*|text()"><xsl:apply-templates></xsl:apply-templates><xsl:element name="xsl:template"><xsl:attribute name="match"><xsl:apply-templates select="." mode="path"></xsl:apply-templates></xsl:attribute><xsl:element name="ok" namespace="http://panax.io/xml/compare"><xsl:attribute name="c:position"><xsl:value-of select="'{count(preceding-sibling::*)+1}'"></xsl:value-of></xsl:attribute><xsl:attribute name="c:name"><xsl:value-of select="'{name()}'"></xsl:value-of></xsl:attribute><xsl:copy-of select="@*"></xsl:copy-of><xsl:element name="xsl:apply-templates"></xsl:element></xsl:element></xsl:element></xsl:template><xsl:template match="*" mode="simple-path"><xsl:param name="position"><xsl:value-of select="count(preceding-sibling::*)+1"></xsl:value-of></xsl:param><xsl:apply-templates select="ancestor::*[1]" mode="simple-path"></xsl:apply-templates><xsl:text>/*</xsl:text><xsl:text>[</xsl:text><xsl:value-of select="$position"></xsl:value-of><xsl:text>]</xsl:text></xsl:template><xsl:template match="*" mode="path"><xsl:param name="position"><xsl:value-of select="count(preceding-sibling::*)+1"></xsl:value-of></xsl:param><xsl:apply-templates select="ancestor::*[1]" mode="simple-path"></xsl:apply-templates><xsl:text>/*</xsl:text><xsl:text>[</xsl:text><xsl:value-of select="$position"></xsl:value-of><xsl:text>]</xsl:text><xsl:text>[local-name()='</xsl:text><xsl:value-of select="local-name()"></xsl:value-of><xsl:text>']</xsl:text><xsl:text>[namespace-uri()='</xsl:text><xsl:value-of select="namespace-uri()"></xsl:value-of><xsl:text>']</xsl:text><xsl:text>[1=1 </xsl:text><xsl:for-each select="@*"><xsl:variable name="value"><xsl:text>concat('',</xsl:text><xsl:call-template name="escape-quot"><xsl:with-param name="string"><xsl:value-of select="." disable-output-escaping="yes"></xsl:value-of></xsl:with-param></xsl:call-template><xsl:text>)</xsl:text></xsl:variable><xsl:value-of select="concat(' and @',name(.),'=',$value)"></xsl:value-of></xsl:for-each><xsl:text>]</xsl:text></xsl:template><xsl:template match="text()" mode="path"><xsl:param name="position"><xsl:value-of select="count(preceding-sibling::*)+1"></xsl:value-of></xsl:param><xsl:apply-templates select="ancestor::*[1]" mode="simple-path"></xsl:apply-templates><xsl:text>/text()</xsl:text><xsl:text>[</xsl:text><xsl:value-of select="$position"></xsl:value-of><xsl:text>]</xsl:text><xsl:variable name="unescaped-value"><xsl:value-of select="." disable-output-escaping="yes"></xsl:value-of></xsl:variable><xsl:variable name="value"><xsl:text>concat('',</xsl:text><xsl:call-template name="escape-quot"><xsl:with-param name="string"><xsl:value-of select="." disable-output-escaping="yes"></xsl:value-of></xsl:with-param></xsl:call-template><xsl:text>)</xsl:text></xsl:variable><xsl:value-of select="concat(&quot;[.=&quot;,$value,&quot;]&quot;)"></xsl:value-of></xsl:template></xsl:stylesheet>`));
    if (stop_at_first_change) {
        xsl_compare.selectSingleNode('//c:change/xsl:apply-templates').remove();
    }

    let details = xdom.xml.transform(document2, xsl_compare)
    return details;
}

xdom.xml.update = function (attribute_ref, value) {
    if (typeof (attribute_ref.textContent) != "undefined") {
        attribute_ref.textContent = value;
    } else if (typeof (attribute_ref.text) != "undefined") {
        node_attribute.text = value;
    }
}

Object.defineProperty(xdom.session, 'loadSession', {
    value: async function (session_id) {
        session_id = (session_id || prompt("Introduzca el id de la sesión", (xdom.session.last_id || "")));
        if (!session_id) return;
        xdom.session.last_id = session_id;

        return xdom.data.load(relative_path + xdom.manifest.server["endpoints"]["loadSession"] + (session_id ? "?sessionId=" + session_id : "")).then(session_document => {
            var stylesheets = session_document.stylesheets;
            if (!stylesheets.length && (session_document.documentElement || document.createElement('p')).getAttribute('controlType')) {
                new xdom.Store('<?xml-stylesheet type="text/xsl" href="' + session_document.getAttribute('controlType').toLowerCase() + '.xslt"?>' + xdom.xml.toString(session_document), function () {
                    xdom.stores.active = this.document;
                    //xdom.dom.refresh();
                    return;
                });
            } else {
                xdom.stores.active = session_document;
                return;
                //xdom.dom.refresh();
            }
        });
    },
    writable: false, enumerable: false, configurable: false
});

Object.defineProperty(xdom.session, 'saveSession', {
    value: function (data, settings) {
        var data = (data || xdom.stores.active)

        prepareData = function (data) {
            if (settings["prepareData"] && settings["prepareData"].apply) {
                settings["prepareData"].apply(this, [data])
            };
            return data;
        }

        var settings = (settings || {});
        //settings = xdom.json.merge(settings, { contentType: 'application/json' });
        let session_id = prompt("Introduzca el id de la sesión", (xdom.session.last_id || xdom.stores.active.documentElement.getAttribute("x:id")));
        if (!session_id) return;
        xdom.session.last_id = session_id;
        var xhr_settings = xdom.json.merge({ headers: { "Accept": 'application/json', "Content-Type": 'text/xml', "X-File-Name": session_id } }, (xhr_settings || settings["xhr"]));
        var onsuccess = (xhr_settings["onsuccess"] || function () { });
        if (data) {
            prepareData(data);
            var xhr = new xdom.xhr.Request(xdom.manifest.server["endpoints"]["saveSession"] + '?test=true', xdom.json.merge(xhr_settings, {
                onSuccess: function () {
                    console.log(xdom.xml.toString(this.xmlDocument));
                }
            }));
            var payload = xdom.xml.createDocument(xdom.xml.toString(data));
            var nodes = payload.selectNodes('//source:value|//@source:value');
            //var nodes = payload.selectNodes('//Option|//source:value');
            //xdom.data.remove(nodes);
            xhr.send(payload);
            //xhr.upload.onprogress = p => {
            //    console.log(Math.round((p.loaded / p.total) * 100) + '%');
            //}

        }
    },
    writable: false, enumerable: false, configurable: false
});

Object.defineProperty(xdom.session, 'live', {
    value: function (live) {
        xdom.session.live.running = live;
        var refresh_rate, document_name_or_array;
        var a = 0;
        if (this.Interval) window.clearInterval(this.Interval);
        if (!live) return;

        refresh_rate = (refresh_rate || 5);
        refresh_rate = (refresh_rate * 1000);
        var refresh = function () {
            window.console.info('Checking for changes in session...');
            xdom.session.loadSession();
        };

        this.Interval = setInterval(function () { refresh() }, refresh_rate);
    },
    writable: false, enumerable: false, configurable: false
});

xdom.data.load = async function (file_name_or_document, custom_on_complete) {
    let document = await xdom.stores.fetch(file_name_or_document)
    xdom.stores.active = document;
    return xdom.stores.active;

    if (!file_name_or_document) return null;
    var library = (arguments[2] || {});
    var _parent = this;
    var _parent_arguments = this;
    on_complete = async function (document, document_name, request) {
        ////var transforms = xdom.data.getTransformationFileName().split(";");
        ////var dom;
        ////var layout_transform = transforms.pop();
        ////for (let t = 0; t < transforms.length; t++) {
        ////    dom = xdom.xml.transform((dom || xdom.stores.active), xdom.library[transforms[t]]);
        ////}
        ////xdom.stores.active = xdom.xml.createDocument(dom || xdom.stores.active);
        //for (let d in xdom.library) {
        //    if (!xdom.library[d]) {
        //        console.info("Document " + d + " not ready");
        //        return;
        //    }
        //}
        await xdom.data.processDocument(document);
        library[document_name] = document;

        if (custom_on_complete && custom_on_complete.apply) {
            custom_on_complete.apply(this, [document, request]);
        }
        else {
            if (typeof (file_name_or_document) == 'string') {
                xdom.stores.active = document;
            }

            //xdom.dom.refresh({ forced: true });
            //xdom.dom.refresh(xdom.dom.target, function () {
            //    xdom.data.history.undo = [xdom.stores.active];
            //    xdom.data.history.redo = [];
            //});
        }; //xdom.dom.refresh
    };
    if (typeof (file_name_or_document) == 'object') {
        var document_name_or_array = file_name_or_document;
        if (document_name_or_array.constructor === [].constructor || document_name_or_array.constructor === {}.constructor) {
            var batch_on_complete = function () {
                for (let document_index in document_name_or_array) {
                    var document_name = document_name_or_array.constructor === {}.constructor ? document_index : document_name_or_array[document_index];
                    if (!library[document_name]) {
                        return;
                    }
                }
                if (custom_on_complete && custom_on_complete.apply) {
                    custom_on_complete.apply(this, [library]);
                };
            }
            var all_loaded = true;
            for (let document_index in document_name_or_array) {
                document_name = document_name_or_array.constructor === {}.constructor ? document_index : document_name_or_array[document_index];
                if (!document_name) continue;
                library[document_name] = library[document_name];
            }
            for (let document_name in library) {
                if (!library[document_name]) {
                    all_loaded = false;
                    xdom.data.load(document_name, batch_on_complete, library);
                }
            }
            if (all_loaded) {
                batch_on_complete.apply(_parent, _parent_arguments)
            }
            return;
        }
        if (typeof (file_name_or_document.selectSingleNode) == 'undefined') return;
        xdom.stores.active = file_name_or_document;
        await xdom.data.loadDependencies(xdom.stores.active, on_complete);
        xdom.session.setData(xdom.xml.toString(xdom.stores.active));
        if (on_complete && on_complete.apply) {
            on_complete.apply(this, arguments);
        }; //xdom.dom.refresh
    } else if (typeof (file_name_or_document) == 'function') {
        file_name_or_document.call(this)
        return;
    } else {
        file_name_or_document = (file_name_or_document || xdom.stores);
        //if (typeof (Storage) !== "undefined") {
        //    if (xdom.session.getKey("xdom.data")) {
        //        xdom.stores.active = xdom.session.getKey("xdom.data");
        //        xdom.data.loadDependencies(xdom.stores.active, on_complete);
        //        if (on_complete && on_complete.apply) {
        //            on_complete.apply(this, arguments);
        //        }; //xdom.dom.refresh(xdom.dom.target);
        //    }
        //} else {
        //    console.log('No support for client storage, go ahead with caution');
        //}
    }

    //if (!xdom.stores.active) {
    if (typeof (file_name_or_document) == "string") {
        xdom.xhr.loadXMLFile(file_name_or_document, {
            onSuccess: function (Response, Request) {
                if (Response.type == "xml") {
                    xdom.Store(Response.responseText, function () {
                        if (on_complete && on_complete.apply) {
                            on_complete.apply(_parent, [this/*.document*/, Request.request, Request]);//_parent_arguments
                        };
                    });
                }
            }
            , onException: function (Response, Request) {
                if (Response.type == "xml") {
                    if (Response.document.documentElement.selectSingleNode('/x:message')) {
                        xdom.stores.active.documentElement.appendChild(Response.document.documentElement);
                    }
                    xdom.Store(Response.responseText, function () {
                        if (on_complete && on_complete.apply) {
                            on_complete.apply(_parent, [this/*.document*/, Request.request, Request]);//_parent_arguments
                        };
                    });
                }
            }
            , onFail: function (Response, Request) {
                xdom.data.load('default.xml');
                xdom.stores.active.reseed();
                if (Response.type == "xml") {
                    if (Response.document.documentElement.selectSingleNode('/x:message')) {
                        xdom.stores.active.documentElement.appendChild(Response.document.documentElement);
                    }
                    xdom.Store(Response.responseText, function () {
                        if (on_complete && on_complete.apply) {
                            on_complete.apply(_parent, [this/*.document*/, Request.request, Request]);//_parent_arguments
                        };
                    });
                } else {
                    if (on_complete && on_complete.apply) {
                        on_complete.apply(_parent, [this/*.document*/]);
                    };
                }
            }
        });
    } else { //Reubicar esta llamada
        xdom.data.load('default.xml');
        xdom.stores.active.reseed();
        //xdom.session.getUserId();
    }
    //} else {
    //    if (on_complete && on_complete.apply) {
    //        on_complete.apply(this, arguments);
    //    }; //xdom.dom.refresh
    //}
}

xdom.data.reload = function (settings) {
    /*var settings = (settings || {})
    xdom.xhr.loadXMLFile('data.xml?id=' + Math.random(), {
        onSuccess: function () {
            xdom.stores.active = xdom.xml.createDocument(this.document);
            if (settings.onSuccess) { settings.onSuccess.apply(this, [this.response, this.xhr]); }
            xdom.dom.refresh(xdom.dom.target);
        }
    });*/
    xdom.data.clear();
    xdom.data.load();
}

xdom.data.clear = function (filter_by) {
    xdom.stores.active = null;
    xdom.data.binding.sources = {};
    xdom.session.setData(null);
    xdom.dom.refresh();
}

xdom.data.sortBy = function (sort_by, is_number, sort_order) {
    var sort_order = (sort_order || 'ascending')
    var start_date = new Date();
    var xsl_transform = xdom.xml.createDocument('\
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:x="http://panax.io/xdom">                 \
  <xsl:output method="xml" indent="no"/>                                                        \
  <xsl:template match="@* | node()">                                                            \
      <xsl:copy>                                                                                \
          <xsl:apply-templates select="@* | node()"/>                                           \
      </xsl:copy>                                                                               \
  </xsl:template>                                                                               \
  <xsl:template match="/*">                                                                     \
    <xsl:copy>                                                                                  \
      <xsl:copy-of select="@*"/>                                                                \
      <xsl:attribute name="sortBy">' + sort_by + '</xsl:attribute>                              \
      <xsl:attribute name="sortOrder">' + sort_order + '</xsl:attribute>                        \
        <xsl:apply-templates select="*">                                                        \
          <xsl:sort select="*[@ref=\'' + sort_by + '\']/@value" order="' + sort_order + '" data-type="' + (is_number ? 'number' : 'text') + '"/>\
          <xsl:sort select="*[local-name()=\'' + sort_by + '\']/@value" order="' + sort_order + '" data-type="' + (is_number ? 'number' : 'text') + '"/>\
        </xsl:apply-templates>                                                                   \
    </xsl:copy>                                                                                 \
  </xsl:template>                                                                               \
</xsl:stylesheet>                                                                               \
', 'text/xml');
    xdom.stores.active = xdom.xml.transform(xdom.stores.active, xsl_transform);
    if (xdom.debug["xdom.data.sortBy"]) {
        console.log("@sort#Transformation Time: " + (new Date().getTime() - start_date.getTime()));
    }
    var e = event;
    if (typeof e.stopPropagation != "undefined") {
        e.stopPropagation();
    } else if (typeof e.cancelBubble != "undefined") {
        e.cancelBubble = true;
    }
    xdom.dom.refresh();
}

xdom.data.getValue = function (element) {
    if (!element || !(element instanceof HTMLElement)) return undefined;
    var element_value = '';
    if ((typeof element) == 'object') {
        /*if ($(element).is('.texto, .text')) {
            element_value = trimAll(element.value !== undefined ? element.value : (element.innerText !== undefined ? element.innerText : element));
            return element_value;
        }
        else */if (element.length && element[0].nodeName.toLowerCase() == 'input' && (element[0].type == "radio" || element[0].type == "checkbox")) {
            for (s = 0; s < element.length; ++s) {
                element_value += (element(s).checked ? (element_value != '' ? ',' : '') + (element(s).value) : '')
            }
        }
        else if (element.options && element.nodeName.toLowerCase() == 'select') {
            for (s = 0; s < element.options.length; ++s) {
                element_value += (element.options[s].selected ? (element_value != '' ? ', ' : '') + (element.className == 'catalog' && (element.value.toUpperCase() == 'TODOS' || element.value.toUpperCase() == 'TODAS') ? 'all' : element.options[s].value) : '')
            }
        }
        else if (element.length) {
            return element
        }
        else if (element.nodeName.toLowerCase() == 'input' && element.type == "checkbox" && element.id != 'identifier') {
            element_value = (element.checked || element.DefaultIfUnchecked && eval(element.DefaultIfUnchecked)) ? element.value : '';
        }
        else if (element.nodeName.toLowerCase() == 'div') {
            element_value = element.innerHTML;
        }
        else {
            element_value = xdom.string.trim(element.value !== undefined ? element.value : (element.innerText !== undefined ? element.innerText : element));
        }
    }
    else {
        element_value = element;
    }
    if (element.className && element.className.match(/\bmoney\b/gi)) {
        element_value = element_value.replace(/[^\d\.-]/gi, '');
    }
    //if (isDateType(element_value)) element_value = fillDate(element_value);
    //if (isNumericOrMoney(element_value) || element_value.isPercent()) element_value = unformat_currency(element_value);
    //if (isNumber(element_value) && !esVacio(element_value)) element_value = parseFloat(element_value)

    return element_value;
}

xdom.data.update = function (target, attribute, value, refreshDOM, action, on_success, srcElement) {
    var srcElement = ((event || {}).srcElement || (xdom.data.update.caller && xdom.data.update.caller.hasOwnProperty('arguments') ? xdom.data.update.caller.arguments[0] : {}).srcElement);
    if (srcElement && srcElement.classList) {
        srcElement.classList.add("working");
    }
    if ([].constructor == (attribute || '').constructor) {
        attribute.map(attr => {
            xdom.data.update(target, attr, undefined, refreshDOM, action, on_success, srcElement);
        })
    } else if ((target || '').constructor === [].constructor) {
        target.map(el => xdom.data.update(el));
    } else if ((target || '').constructor === {}.constructor || typeof (target) === 'object' && typeof (target.selectSingleNode) == 'undefined') {
        if (target["target"] || (target["attributes"] || '').constructor == [].constructor) {
            var { target, attributes, value, refreshDOM, action } = target;
            xdom.data.update(target, attributes, value, refreshDOM, action, on_success, srcElement);
        } else if (srcElement) {
            real_target = target["target"] || ((xdom.dom.findClosestElementWithId(srcElement) || srcElement).id);
            Object.entries(target).map(([key, value]) => {
                xdom.data.update(real_target, key, value, refreshDOM, action, on_success, srcElement);
            })
        }
        else {
            throw 'Invalid target'
        }
    } else {
        target = (!target && (srcElement || {}).source || (target.ownerDocument || {}).store && target || (srcElement || {}).store && (srcElement || {}).store.find(target) || xdom.stores.find(target));
        attribute = xdom.data.coalesce(attribute, "@value");
        value = xdom.data.coalesce(value, (value === undefined && attribute == '@value' ? xdom.data.getValue(srcElement) : undefined));
        let targets = target; //xdom.stores.find(target);
        if ({}.constructor == attribute.constructor) {
            Object.entries(attribute).forEach(([attrib, value]) => {
                xdom.data.update(target, attrib, value, refreshDOM, action, on_success, srcElement);
            });
        } else if (attribute.match(/^@/)) {
            targets.setAttribute(attribute.replace(/^@/, ''), value, refreshDOM).then(() => {
                //console.log('Ready xdom.data.update');
                (srcElement instanceof Element && srcElement || document.createElement('p')).classList.remove("working");
            });
        } else if (attribute == 'text()') {
            targets.textContent(attribute, value);
        } else if (attribute == 'after()') {
            targets.appendAfter(attribute, value);
        } else if (attribute == 'before()') {
            targets.appendBefore(attribute, value);
        } else if (attribute == 'node()') {
            targets.insertBefore(attribute, value);
        } else {
            throw 'Invalid command'
        }
    }
}

xdom.tools.isNullOrEmpty = function (string_or_object) {
    var sValue = xdom.string.trim(xdom.data.getValue(string_or_object))
    return (sValue == '' || sValue == null || sValue == undefined);
}

xdom.deprecated.checkStatus = function () {
    var attributes = xdom.stores.active.selectNodes('//@*[namespace-uri()!="http://panax.io/xdom/xhr"]');
    if (Object.keys(xdom.xhr.Requests).length == 0) {
        xdom.data.remove(attributes);
        xdom.dom.refresh();
    }
}

xdom.xhr.Response = function (xhr) {
    if (!(this instanceof xdom.xhr.Response)) return new xdom.xhr.Response(xhr);
    //TODO: Que el usuario pueda activar o desactivar el "escape"
    if (xhr.responseXML && xhr.responseXML.firstElementChild.namespaceURI != 'http://www.w3.org/1999/XSL/Transform' && xhr.responseText.replace(/(\>|^)[\r\n\s]+(\<|$)/ig, "$1$2").match(/\r\n/)) { /*xhr.responseXML no escapa saltos de línea cuando vienen en atributos, por eso se reemplazan por la entidad.*/
        this.responseText = xhr.responseText.replace(/(\>|^)[\r\n\s]+(\<|$)/ig, "$1$2").replace(/\r\n/ig, "&#10;");
        this.responseXML = xdom.xml.createDocument(this.responseText);
    } else {
        this.responseText = xhr.responseText;
        this.responseXML = xhr.responseXML;
    }
    var value;
    Object.defineProperty(this, 'headers', {
        get: function get() {
            var _all_headers = xhr.getAllResponseHeaders().split(/[\r\n]+/);
            _headers = {}
            for (let h in _all_headers) {
                var header = _all_headers[h].split(/\s*:\s*/);
                if (!(header.length > 1)) {
                    continue;
                }
                _headers[header.shift().toLowerCase()] = header.join(":");
            }
            return _headers;
        }
    });
    Object.defineProperty(this, 'status', {
        get: function get() {
            return xhr.status;
        }
    });
    Object.defineProperty(this, 'document', {
        get: function get() {
            switch (this.type) {
                case "xml":
                case "html":
                    return xdom.xml.createDocument(this.responseXML || this.responseText);
                    break;
                default:
                    return this.responseText;
            }
        }
    });
    Object.defineProperty(this, 'value', {
        get: function get() {
            switch (this.type) {
                case "json":
                    var json
                    try {
                        json = JSON.parse(this.responseText);
                    } catch (e) {
                        json = eval("(" + this.responseText + ")");
                    }
                    return json
                    break;
                case "xml":
                case "html":
                    return this.document;
                    break;
                case "script":
                    try {
                        var return_value;
                        eval('return_value = new function(){' + this.responseText + '\nreturn this;}()');
                        return return_value;
                    } catch (e) {
                        return null;
                    }
                default:
            }
        }
    });
    Object.defineProperty(this, 'type', {
        get: function get() {
            if ((this.headers["content-type"] || "").indexOf("json") != -1 || xdom.tools.isJSON(this.responseText)) {
                return "json";
            } else if ((this.headers["content-type"] || "").indexOf("xml") != -1 || this.responseXML && this.responseXML.documentElement || this.responseText.indexOf("<?xml ") >= 0) {
                return "xml"
            } else {
                if (this.responseText.toUpperCase().indexOf("<HTML") != -1) {
                    return "html";
                } else {
                    return "script";
                }
            }
        }
    });
    Object.defineProperty(this, 'contentType', {
        get: function get() {
            return this.headers["content-type"].split(";")[0];
        }
    });
    Object.defineProperty(this, 'charset', {
        get: function get() {
            return this.headers["content-type"].split(";")[1];
        }
    });
    switch (this.type) {
        case "json":
            Object.defineProperty(this, 'json', {
                get: function get() {
                    var json
                    try {
                        json = JSON.parse(this.responseText);
                    } catch (e) {
                        json = eval("(" + this.responseText + ")");
                    }
                    return json;
                }
            });
            break;
        case "script":
            Object.defineProperty(this, 'object', {
                get: function get() {
                    try {
                        var return_value;
                        eval('return_value = new function(){' + this.responseText + '\nreturn this;}()');
                        return return_value;
                    } catch (e) {
                        return null;
                    }
                }
            });
        default:
    }
    return this;
}
xdom.xhr.Request = function (request, settings) {
    if (!(this instanceof xdom.xhr.Request)) return new xdom.xhr.Request(request, settings);
    var element = this
    var settings = (settings || {});
    this.settings = settings;
    this.xhr = null;

    var _onSuccess = function () {
        //if (this.subscribers_ARRAY.length) {
        //    for (let s = this.subscribers_ARRAY.length - 1; s >= 0; s--) {
        //        var src = this.subscribers_ARRAY.pop()
        //        this.requester = src["subscriber"]
        //        this.onSuccess.apply(this, [this.Response, this]);
        //        if (src.onSuccess && src.onSuccess.apply) {
        //            src.onSuccess.apply(src["subscriber"], [this.Response, this]);
        //        }
        //        //if (src && src.nodeType == 2 && src.namespaceURI == "http://panax.io/fetch/request") {
        //        //    xdom.data.remove(src);
        //        //}
        //    }
        //} else {

        //}
        if (Object.keys((this.subscribers || {})).length) {
            for (let s in this.subscribers) {
                var src = this.subscribers[s];
                this.requester = src["subscriber"]
                this.onSuccess.apply(this, [this.Response, this]);
                if (src && src.settings && src.settings.onSuccess && src.settings.onSuccess.apply) {
                    src.settings.onSuccess.apply(src["subscriber"], [this.Response, this]);
                }
            }
        } else {
            this.onSuccess.apply(this, [this.Response, this]);
        }
    };

    var _onComplete = function () {
        delete this.srcElement.xhr;
        /*onComplete will do a final chante to perform any action with the subscriber even if it doesn't exist anymore in the current document. In such case it will be unsubscribed automatically*/
        if (Object.keys((this.subscribers || {})).length) {
            for (let s in this.subscribers) {
                var src = this.subscribers[s];
                this.requester = src["subscriber"]
                this.onComplete.apply(this, [this.Response, this]);
                if (src && src.settings && src.settings.onComplete && src.settings.onComplete.apply) {
                    src.settings.onComplete.apply(src["subscriber"], [this.Response, this]);
                }
                //if (src["subscriber"] && !(xdom.data.find(this.requester))) {
                //    this.unsubscribe(src);
                //}
            }
        } else {
            this.onComplete.apply(this, [this.Response, this.xhr]);
        }
    };

    var _onAbort = function () {
        if (Object.keys((this.subscribers || {})).length) {
            for (let s in this.subscribers) {
                var src = this.subscribers[s];
                this.requester = src["subscriber"]
                this.onAbort.apply(this, [this.Response, this]);
                if (src.onAbort && src.onAbort.apply) {
                    src.onAbort.apply(src["subscriber"], [this.Response, this]);
                }
            }
        } else {
            this.onAbort.apply(this, [this.Response, this.xhr]);
        }
    };

    var _onFail = function () {

    };

    var _onException = function () {
        if (this.xhr.status == 401) {
            //xdom.session.setUserId(null);
            xdom.session.updateSession({ "status": "unauthorized" });
            if (!xdom.session.retrying && xdom.session.getUserLogin() == "guest") {
                xdom.session.retrying = true;
                xdom.session.login(xdom.session.getUserLogin());
                this.send(this.payload);
                xdom.delay(500).then(() => {
                    xdom.session.retrying = undefined
                });
            }
            xdom.dom.refresh();
        }
        if (this.xhr.status == 304) {
            alert("No hay cambios");
        }
        console.error((this.Response.json || {}).message || this.responseStatus[1] || this.xhr.responseText);
    };

    var _Subscriber = function (subscriber, id, settings) {
        Object.defineProperty(this, 'subscriber', {
            value: subscriber,
            writable: true, enumerable: false, configurable: false
        });
        Object.defineProperty(this, 'id', {
            value: id,
            writable: true, enumerable: false, configurable: false
        });
        Object.defineProperty(this, 'settings', {
            value: settings,
            writable: true, enumerable: false, configurable: false
        });
    }

    this.srcElement = (settings["srcElement"] || (event || {}).srcElement || {});
    this.src = settings["src"];
    this.subscribers = (settings["subscribers"] || {});
    this.target = (settings["target"] || {})
    this.request = request;
    this.headers = (settings["headers"] || {})
    this.method = (settings["method"] || "GET");
    this.contentType = settings["contentType"];
    this.argumentSeparator = (settings["argumentSeparator"] || "&");
    this.url = (settings["url"] || "");
    this.urlQuery = (settings["urlQuery"] || "");
    this.encodeURIString = (settings["encodeURIString"] !== undefined ? settings["encodeURIString"] : true);
    this.parameters = (settings["parameters"] || new Object());
    this.async = (settings["async"] !== undefined ? settings["async"] : true);
    this.request_id = xdom.data.coalesce(settings["request_id"]);
    if (this.advise) element.advise.removeNode(true);
    this.advise = null;

    this.resetData = function () {
        this.url = (this.settings["url"] || "");
        this.status = 'initialized';
        this.responseStatus = new Array(2);
        this.document = undefined;
        this.Response = {};
        this.Request = this;
    };

    this.getResultType = function () { return (element.xmlDocument ? 'xml' : (element.htmlDocument ? 'html' : (element.script ? 'script' : undefined))) }

    var _onSubscribe = function (subscription) {
        if (subscription && subscription.onSubscribe && subscription.onSubscribe.apply) {
            subscription.onSubscribe.apply(subscription["subscriber"], [this.Response, this]);
        }
    };

    this.subscribe = function (subscriber, settings) {
        if (!subscriber) return false;
        var s
        if (subscriber.constructor === {}.constructor) { //is an object
            s = new _Subscriber(subscriber, subscriber.id, settings);
        } else if (typeof (subscriber.selectSingleNode) != 'undefined') { //is a node or an attribute
            if (subscriber.nodeType == 2) {
                s = new _Subscriber(subscriber, (subscriber.ownerElement || document.createElement('p')).getAttribute("x:id") + '/@' + subscriber.name, settings)
            } else {
                s = new _Subscriber(subscriber, (subscriber || document.createElement('p')).getAttribute("x:id"), settings);
            }
        }
        this.subscribers[s.id] = s;
        _onSubscribe(this.subscribers[s.id]);
        return this.subscribers[s.id];
    }

    var _onUnsubscribe = function (subscription) {
        if (subscription && subscription.onUnsubscribe && subscription.onUnsubscribe.apply) {
            subscription.onUnsubscribe.apply(subscription["subscriber"], [this.Response, this]);
        }
    };

    this.unsubscribe = function (subscription) {
        if (!subscription) return false;
        var id;
        if (subscription instanceof _Subscriber) {
            id = subscription["id"];
        } else {
            if (subscription.constructor === {}.constructor) { //is an object
                id = subscription["id"];
            } else if (typeof (subscription.selectSingleNode) != 'undefined') { //is a node or an attribute
                id = (subscription.ownerElement || document.createElement('p')).getAttribute("x:id") + '/@' + subscription.name;
            }
        }
        _onUnsubscribe(this.subscribers[id]);
        delete this.subscribers[id];
    }

    //var updateSubscribers = function () {
    //    if (Object.keys((element.subscribers || {})).length) {
    //        for (let s in element.subscribers) {
    //            var src = element.subscribers[s];
    //            var requester = xdom.data.find(src["subscriber"]);
    //            var subscriberNode = requester.ownerElement;
    //            if (subscriberNode) {
    //                subscriberNode.setAttribute(requester.prefix.replace(/^request$/, "requesting") + ":" + (requester.baseName || requester.localName),'true');
    //                xdom.dom.refresh();
    //            }
    //            //if (src["subscriber"] && !(xdom.data.find(this.requester))) {
    //            //    this.unsubscribe(src);
    //            //}
    //        }
    //    }
    //}

    this.abort = function () {
        if (this.xhr) {
            this.xhr.abort();
        }
    }

    this.onLoading = function () { };
    this.onLoaded = function () { };
    this.onInteractive = function () { };
    this.onComplete = (settings.onComplete || function () { });
    this.onAbort = (settings.onAbort || function () { });
    this.onSuccess = (settings.onSuccess || function () { });
    this.onException = (settings.onException || function () { });
    this.onFail = (settings.onFail || function () {
        console.warn('Server is not available: ' + this.request);
    });

    this.createXHR = function () {
        try {
            this.xhr = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e1) {
            try {
                this.xhr = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e2) {
                this.xhr = null;
            }
        }

        if (!this.xhr) {
            if (typeof XMLHttpRequest != "undefined") {
                this.xhr = new XMLHttpRequest();
            } else {
                this.failed = true;
            }
        }
        var element = this;

        this.xhr.onabort = function () {
            this.status = 'aborted';
            _onAbort.apply(element, [element.Response, element]);
            _onComplete.apply(element, [element.Response, element]);
        }

        this.xhr.onreadystatechange = function () {
            switch (element.xhr.readyState) {
                case 1:
                    element.status = 'loading';
                    element.onLoading();
                    break;
                case 2:
                    element.status = 'loaded';
                    element.onLoaded();
                    break;
                case 3:
                    element.status = 'interactive';
                    element.onInteractive();
                    break;
                case 4:
                    element.status = 'complete';
                    element.document = this.payload;

                    var success = (element.xhr.status > 0 && element.xhr.status < 300);

                    element.Response = new xdom.xhr.Response(element.xhr);

                    //if (element.response.type == "xml") {
                    //    var stylesheets = element.response.document.selectNodes("processing-instruction('xml-stylesheet')");
                    //    for (let s = 0; s < stylesheets.length; ++s) {
                    //        stylesheet = JSON.parse('{' + (stylesheets[s].data.match(/(\w+)=(["'])([^\2]+?)\2/ig) || []).join(", ").replace(/(\w+)=(["'])([^\2]+?)\2/ig, '"$1":$2$3$2') + '}');
                    //        if (!xdom.library[stylesheet.href] && !xdom.xhr.Requests[stylesheet.href]) {
                    //            var oData = new xdom.xhr.Request(stylesheet.href);
                    //            oData.onComplete = element.onComplete;
                    //            oData.onSuccess = element.onSuccess;
                    //            oData.onException = element.onException;
                    //            oData.load();
                    //            return;
                    //        }
                    //    }
                    //}

                    if (success) {
                        if (element.xhr.status == 204) {
                            alert('El servidor terminó sin regresar resultados')
                        }
                        _onSuccess.apply(element, [element.Response, element]);
                    } else {
                        switch (element.xhr.status) {
                            case 0: /*Cuando se aborta una consulta regresa 0*/
                                break;
                            case 404:
                            case 503:
                                _onFail.apply(element, [element.Response, element]);
                                element.onFail.apply(element, [element.Response, element]);
                                break;
                            case 500:
                            case 401:
                                _onException.apply(element, [element.Response, element]);
                                element.onException.apply(element, [element.Response, element]);
                                break;
                            default:
                                _onException.apply(element, [element.Response, element]);
                                element.onException.apply(element, [element.Response, element]);
                                break;
                        }
                        element.createXHR(); //recreate xhr request
                    }

                    //element.url = "";
                    _onComplete.apply(element, [element.Response, element]);
                    break;
            }
        };
    };

    this.setParameter = function (name, value) {
        this.parameters[name] = value;//Array(value, false);
    };

    this.encParameter = function (name, value, returnparameters) {
        if (true == returnparameters) {
            return Array(Encoder.urlEncode(name), Encoder.urlEncode(value));
        } else {
            this.parameters[Encoder.urlEncode(name)] = Array(Encoder.urlEncode(value), true);
        }
    }

    this.processURLString = function (string, encode) {
        encoded = Encoder.urlEncode(this.argumentSeparator);
        regexp = new RegExp(this.argumentSeparator + "|" + encoded);
        parameterArray = string.split(regexp);
        for (let i = 0; i < parameterArray.length; i++) {
            urlParameters = parameterArray[i].split("=");
            if (true == encode) {
                this.encParameter(urlParameters[0], urlParameters[1]);
            } else {
                this.setParameter(urlParameters[0], urlParameters[1]);
            }
        }
    }

    this.createURLString = function (urlstring) {
        if (this.encodeURIString && this.url.length) {
            this.processURLString(this.url, true);
        }

        var originalURL = this.request
        var urlName = this.request.match(/(.*?)(?=\?)/);
        if (urlName == null) {
            this.url = originalURL;
        }
        else {
            this.url = urlName[0];
        }

        if (urlstring) {
            if (this.url.length) {
                this.url += this.argumentSeparator + urlstring;
            } else {
                this.url = urlstring;
            }
        }

        var m = (originalURL + "&").match(/(?:[&\?])(@?\w+)=(.*?)(?=&)/g);
        if (m == null) {
            //alert("No match");
        } else {
            var s = "Match at position " + m.index + ":\n";
            for (let i = 0; i < m.length; i++) {
                var parameterName = (m[i] + "&").replace(/(?:[&\?])(@?\w+)=(.*?)[\&\?]/g, '$1');
                var parameterValue = (m[i] + "&").replace(/(?:[&\?])(@?\w+)=(.*?)[\&\?]/g, '$2');
                // alert(m[i]+'\n'+parameterName+'\n'+parameterValue)
                this.setParameter(parameterName, parameterValue);
            }
        }

        // prevents caching of URLString
        this.setParameter("rndval", new Date().getTime());

        urlstringtemp = new Array();
        for (key in this.parameters) {
            if (this.parameters[key] && this.parameters[key].constructor === [].constructor) {
                //if (false == this.parameters[key][1] && true == this.encodeURIString) {
                //    encoded = this.encParameter(key, this.parameters[key][0], true);
                //    delete this.parameters[key];
                //    this.parameters[encoded[0]] = Array(encoded[1], true);
                //    key = encoded[0];
                //}
                urlstringtemp[urlstringtemp.length] = Encoder.urlEncode(key) + "=" + Encoder.urlEncode(this.parameters[key][0]);
            }
            else {
                urlstringtemp[urlstringtemp.length] = Encoder.urlEncode(key) + "=" + Encoder.urlEncode(this.parameters[key]);
            }
        }

        if (urlstring) {
            this.urlQuery += this.argumentSeparator + urlstringtemp.join(this.argumentSeparator);
        } else {
            this.urlQuery += urlstringtemp.join(this.argumentSeparator);
        }
    }

    this.send = function (payload) {
        this.payload = payload;
        this.load(payload);
    }

    this.post = function (payload) {
        this.method = "POST";
        this.send(payload);
    }

    this.get = async function (settings) {
        this.settings = xdom.json.merge(this.settings, settings);
        this.method = "GET";
        if (this.xhr && this.xhr.onreadystatechange && this.xhr.status > 0) {
            this.xhr.onreadystatechange();
        } else {
            this.send();
        }
    }

    this.load = async function (payload) {

        if (this.headers["Content-Type"] == "text/xml") {
            this.payload = xdom.xml.toString(payload || this.payload).replace(/[^<]*<\?xml\s[^>]+\?>[^<]*/ig, '').replace(/%/g, '%25'); //Revisar si hay que escapar más caracteres
        }
        this.resetData();
        this.createXHR();
        if (this.failed) {
            this.onFail();
        } else {
            //updateSubscribers();
            this.createURLString();
            /*if (this.id) {
                this.context = document.getElementById(this.id);
            }*/
            if (this.xhr) {
                var element = this;
                var url_post

                if (!this.payload) {
                    this.contentType = (this.headers["Content-Type"] || this.contentType || "application/x-www-form-urlencoded")
                } else if (xdom.tools.isJSON(this.payload)) {
                    this.contentType = (this.headers["Content-Type"] || this.contentType || "application/json")
                } else {
                    this.contentType = (this.headers["Content-Type"] || this.contentType || "text/xml")
                }

                if (this.payload) {
                    url_post = this.payload;
                    this.xhr.open("POST", this.url + '?' + this.urlQuery, this.async);
                    this.xhr.setRequestHeader("Content-Type", this.contentType)
                } else if (this.method.toUpperCase() == "POST") {
                    url_post = this.urlQuery;
                    //url_post=encodeURI(url_post)
                    this.xhr.open("POST", this.url, this.async);
                    this.xhr.setRequestHeader('Content-Type', this.contentType);
                } else {
                    url_post = null;
                    var totalurlstring = this.url + '?' + this.urlQuery;
                    this.xhr.open(this.method, totalurlstring, this.async);
                    this.xhr.setRequestHeader("Content-Type", this.contentType)
                    /*try {
                        
                    } catch (e) { }*/
                }
                if (xdom.debug.enabled) {
                    this.xhr.setRequestHeader("X-Debugging", xdom.debug.enabled);
                }
                var _request_headers = this.headers;
                for (let h in _request_headers) {
                    if (h == "Content-Type") continue;
                    this.xhr.setRequestHeader(h, _request_headers[h])
                }

                if (this.payload || this.method == "POST")
                    this.xhr.send(url_post);
                else
                    this.xhr.send(this.url);

                //this.xhr.upload.onprogress = p => {
                //    console.log(Math.round((p.loaded / p.total) * 100) + '%');
                //}
            }
        }
    };

    this.eval = function (Context) {
        var return_value;
        if (this.script) {
            try {
                eval('return_value = new function(){' + this.script + '\nreturn this;}()');
            } catch (e) {
                return null;
            }
            this.Response["json"] = return_value;
            return return_value;
        }
    }
}

xdom.xhr.loadXMLFile = function (sXmlFile, settings) {
    var settings = (settings || {});
    var xhttp = new xdom.xhr.Request(sXmlFile);
    xhttp.method = (settings.method || 'GET');
    xhttp.async = (settings.async == undefined ? true : settings.async);
    xhttp.onSuccess = function () {
        if (settings.onSuccess) {
            settings.onSuccess.apply(this, [this.Response, this]);
        }
    }
    xhttp.onFail = function () {
        if (settings.onFail) {
            settings.onFail.apply(this, [this.Response, this]);
        }
    }
    xhttp.onException = function () {
        if (settings.onException) {
            settings.onException.apply(this, [this.Response, this]);
        }
        if (this.xhr.status >= 500) {
            console.error('Error al descargar contenido XML ' + this.request + ':\n\n ' + this.xhr.responseText + '\n\n')
        }
    };
    xhttp.load();
    return xhttp;
}

xdom.xhr.Exception = function (xhr) {
    this.message = 'Exception';
}

xdom.xhr.upload = function (data, target_name) {
    if (!data) return
    if (data) {
        var xhr = new xdom.xhr.Request(xdom.manifest.server["endpoints"]["upload_xml"] + '?target_name=' + target_name, {
            headers: {
                "Content-Type": 'text/xml'
                , "Accept": 'text/xml'
            }
            , onSuccess: function () {
                console.log("Uploaded file");
            }
        });
        xhr.send(xdom.xml.createDocument(xdom.xml.toString(data)));
        //xhr.upload.onprogress = p => {
        //    console.log(Math.round((p.loaded / p.total) * 100) + '%');
        //}
    }
}

Object.defineProperty(xdom.session, 'getUserId', {
    value: function () {
        var user_id = xdom.session.getKey("userId");
        //var on_complete = function () {
        //    xdom.session.setUserId(user_id);
        //}
        //if (user_id === undefined && !xdom.session.connecting) {
        //    xdom.session.connecting = true;
        //    var oData = new xdom.xhr.Request(relative_path + "xdom/server/session.asp", { method: 'GET', async: false });
        //    oData.onSuccess = function (Response, Request) {
        //        user_id = null;
        //        if (Response.type == "json") {
        //            user_id = (Response.value.userId || null);
        //        }
        //        if (Response.value.message) {
        //            console.error(Response.value.message);
        //        }
        //        //on_complete.apply(this, arguments);
        //    }
        //    oData.onComplete = function () {
        //        xdom.session.connecting = undefined;
        //    }
        //    oData.load();
        //}
        //on_complete.apply(this, arguments);

        return user_id;
    },
    writable: false, enumerable: false, configurable: false
});

Object.defineProperty(xdom.session, 'getUserLogin', {
    value: function () {
        return xdom.session.user_login;
    },
    writable: false, enumerable: false, configurable: false
});

xdom.LoadXMLString = function (xmlString) {
    return xdom.xml.fromString(xmlString);
}
